#!/bin/bash

replaceinfile()
{
    local fnsrc=$1
    local fndst=$2
    local dstr=$3
    local rstr=$4
    
    if [ -f ${fnsrc} ]; then
        sed "s|${dstr}|${rstr}|g" ${fnsrc} > ${fndst}
        return 1
    else
        return 0
    fi
}


VER_GREP=`grep APP_VERSION_STR res/resource.h`
VER_ARR=($VER_GREP)
VER_STR=`echo ${VER_ARR[2]} | sed "s/\"//g"`

SRCRES=res/installerscript.iss.src
DSTRES=iss/installerscript.iss

echo "Generating ${DSTRES} ... "

if [ ! -f ${SRCRES} ];then
    echo "Requires ${SRCRES} file."
    exit 0
fi

replaceinfile ${SRCRES} ${DSTRES} "##VERSION##" ${VER_STR}

echo "done."
