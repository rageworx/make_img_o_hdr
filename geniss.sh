# Windows Inno Setup package installer generator script
# (C)2021 Raphael Kim
#!/bin/bash

# Get version --
VER_GREP=`grep APP_VERSION_STR res/resource.h`
VER_ARR=($VER_GREP)
VER_STR=`echo ${VER_ARR[2]} | sed "s/\"//g"`
ARCH_V=`uname -r | cut -d . -f1`

#PFX cert file may already installed on system.
#PFXCERT="${HOME}/Documents/raphael-cert/RaphaelKim.pfx"
PKGR="pkgbuild --install-location /Applications"
INSTPATH="iss"
APPNAME="MIOHDR"
SRCISS="res/installerscript.iss.src"

if [ ! -e ${SRCISS} ];then
    echo "Source ISS, ${SRCISS} not found."
    exit 0
fi

if [ ! -e ${INSTPATH} ];then
    mkdir -p ${INSTPATH}
fi

replaceinfile()
{
    local fnsrc=$1
    local fndst=$2
    local dstr=$3
    local rstr=$4
    
    if [ -f ${fnsrc} ]; then
        sed "s|${dstr}|${rstr}|g" ${fnsrc} > ${fndst}
        return 1
    else
        return 0
    fi
}

DISTDST="${INSTPATH}/installerscript.iss"

if [ -e ${DISTDST} ];then rm -rf ${DISTDST};fi

echo "Generating ${DISTDST} ... "
replaceinfile ${SRCISS} ${DISTDST} "##VERSION##" ${VER_STR}

echo "done."
