# Makefile for Make Image O' HDR for LLVM and Mac OS X
# ----------------------------------------------------------------------
# Written by Raph.K.
#

# Compiler preset.
CC_PREFIX = llvm-
CC_PATH =

GCC = $(CC_PATH)$(CC_PREFIX)gcc
GPP = $(CC_PATH)$(CC_PREFIX)g++
AR  = ar

# Sens architecture. x86.64 or arm64
ARCH = $(shell uname -m)
KRNL = $(shell uname -s)
KVER = $(shell uname -r | cut -d . -f1)

# Base PATH
BASE_PATH = .
FIMG_PATH = ../fl_imgtk/lib
MINI_PATH = ../minIni/dev
SRC_PATH  = $(BASE_PATH)/src
FSRC_PATH = $(SRC_PATH)/fl
XCOI_PATH = /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Headers

# TARGET settings
TARGET_PKG = MIOHDR
TARGET_DIR = ./bin
TARGET_OBJ = ./obj
TARGET_INC = $(TARGET_DIR)/inc
SOURCE_MAC = ${TARGET_PKG}.app.ref
TARGET_MAC = ${TARGET_PKG}.app

# DEFINITIONS
DEFS  = -D_POSIX_THREADS -DSUPPORT_DRAGDROP -DMININI_ANSI
DEFS += -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_THREAD_SAFE -D_REENTRANT
DEFS += -DNOCPUID -DINI_ANSIONLY

# architecture flag
ARCHFLAGS =
ifeq ($(KRNL),Darwin)
    ifeq ($(shell test $(KVER) -gt 19; echo $$?),0)
        ARCHFLAGS += -arch x86_64 -arch arm64
        ARCHFLAGS += -mmacosx-version-min=11.0
    endif
endif

# Compiler optiops 
COPTS = -ffast-math -fexceptions -O3

# CC FLAG
CFLAGS  = -I$(SRC_PATH) -I$(FSRC_PATH) 
CFLAGS += -I$(MINI_PATH)
CFLAGS += -I$(XCOI_PATH)
CFLAGS += -I$(FIMG_PATH)  -Ires
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)
CFLAGS += $(ARCHFLAGS)

# LINK FLAG
LFLAGS += -L$(FIMG_PATH)
LFLAGS += -lfltk_png
LFLAGS += -lfltk_jpeg
LFLAGS += -lfltk_images
LFLAGS += -lfltk 
LFLAGS += -lfl_imgtk
LFLAGS += -lfltk_z
LFLAGS += -lpthread
LFLAGS += -framework Cocoa -framework quartz -framework Foundation -framework AppKit

# minIni Sources & Objects
MININI_SRC = $(MINI_PATH)/minIni.c
MININI_OBJ = $(TARGET_OBJ)/minIni.o

# Sources
SRCS  = $(wildcard $(SRC_PATH)/*.cpp)
SRCSM = $(wildcard $(SRC_PATH)/*.mm)

# Make object targets from SRCS.
OBJS  = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)
OBJMS = $(SRCSM:$(SRC_PATH)/%.mm=$(TARGET_OBJ)/%.o)

.PHONY: prepare clean testenv

all: prepare continue packaging

continue: $(TARGET_DIR)/$(TARGET_PKG)

testenv:
	@echo "CFLAGS=${CFLAGS}"
	@echo "LFLAGS=${LFLAGS}"
	@echo "KRNL=${KRNL}"
	@echo "KVER=${KVER}"

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o

packaging: $(TARGET_DIR)/$(TARGET_PKG)
	@cp -rf ./res/${SOURCE_MAC} ${TARGET_DIR}/${TARGET_MAC}
	@mkdir -p ${TARGET_DIR}/${TARGET_MAC}/Contents/MacOS
	@cp -rf $< ${TARGET_DIR}/${TARGET_MAC}/Contents/MacOS
	@mkdir -p ${TARGET_DIR}/${TARGET_MAC}/Contents/Resources
	@cp -rf res/*.svg ${TARGET_DIR}/${TARGET_MAC}/Contents/Resources
	@./automacverapply.sh
	@cp -rf LICENSE ${TARGET_DIR}

$(MININI_OBJ): $(MININI_SRC)
	@echo "Building $@ ... "
	@$(GCC) $(CFLAGS) -c $< -o $@
    
$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Building (CXX) $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(OBJMS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.mm
	@echo "Buulding (MM) $@ ... "
	@$(GPP) $(CFLAGS)  -ObjC -c $< -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS) $(OBJMS) $(MININI_OBJ)
	@echo "Generating $@ ..."
	@$(GPP) -g $(TARGET_OBJ)/*.o  $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."
