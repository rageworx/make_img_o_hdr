#ifndef NOCPUID
#include <cpuid.h>
#endif /// of NOCPUID

#define APP_VERSION                             0,2,28,100
#define APP_VERSION_STR                         "0.2.28.100"

#ifndef NOCPUID
#if defined(BUILD_AVX)
    #define RES_FILEDESC    "Make Image O HDR x86_64_AVX"
#elif defined(BUILD_SSE3)
    #define RES_FILEDESC    "Make Image O HDR x86_64_SSE3"
#else
    #define RES_FILEDESC    "Make Image O HDR x86_64"
#endif /// __AVX__
#else /// of NOCPUID
    #define RES_FILEDESC    "Make Image O HDR"
#endif /// of NOCPUID

#define IDC_ICON_A                              101

