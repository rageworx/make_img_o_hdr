# Mac OS package installer generator script
# (C)2022 Raphael Kim
#!/bin/bash

# Get version --
VER_GREP=`grep APP_VERSION_STR res/resource.h`
VER_ARR=($VER_GREP)
VER_STR=`echo ${VER_ARR[2]} | sed "s/\"//g"`
ARCH_V=`uname -r | cut -d . -f1`

#PFX cert file may already installed on system.
#PFXCERT="${HOME}/Documents/raphael-cert/RaphaelKim.pfx"
PKGR="pkgbuild --install-location /Applications"
INSTPATH="macospkg"
PKGSPATH="packages"
APPNAME="MIOHDR"
SRCBIN="bin/${APPNAME}.app"
INSTOPT=

if [ ! -e ${SRCBIN} ];then
    echo "Source binary ${SRCBIN} not found."
    exit 0
fi

#if [ -e ${PFXCERT} ]; then
#    echo "Code signing for application ..."
#    codesign ${SRCBIN} -fv ${PFXCERT}
#fi

if [ ! -e ${INSTPATH} ];then
    mkdir -p ${INSTPATH}
fi

if [ ! -e ${PKGSPATH} ];then
    mkdir -p ${PKGSPATH}
fi

if [ ${ARCH_V} -gt 19 ];then
    INSTOPT="_universal";
else
    INSTOPT="X";
fi

POUTPATH="${PKGSPATH}/${APPNAME}.pkg"
PACKAGEPATH="${INSTPATH}/${APPNAME}_v${VER_STR}_MacOS${INSTOPT}.pkg"
PKGROPT+=" --component ${SRCBIN}"
PKGROPT+=" ${POUTPATH}"
PKGROPT+=" --version \"${VER_STR}\""
PKGROPT+=" --scripts pkgscripts/"

if [ "$1" == "clean" ];then
    echo "Cleaning ..."
    rm -rf ${POUTPATH}
    rm -rf ${PACKAGEPATH}
    echo "Done."
    exit 0
fi

echo "Generating package ... "
${PKGR} ${PKGROPT}

if [ ! -e ${POUTPATH} ]; then
    echo "Error, ${POUTPATH} not generated."
    exit 0
fi

if [ -e ${PACKAGEPATH} ];then
    rm -rf ${PACKAGEPATH}
fi

replaceinfile()
{
    local fnsrc=$1
    local fndst=$2
    local dstr=$3
    local rstr=$4
    
    if [ -f ${fnsrc} ]; then
        sed "s|${dstr}|${rstr}|g" ${fnsrc} > ${fndst}
        return 1
    else
        return 0
    fi
}

DISTSRC="res/distribution.source"
DISTDST="pkgscripts/distribution.dist"
PB="productbuild"
PBOPT=" --distribution ${DISTDST}"
PBRES=" --resources pkgscripts/"
PBPKG=" --package-path packages/"

echo "Preparing Distribution ..."
#cp -rf ${DISTSRC} ${DISTDST}
rm -rf ${DISTDST} > /dev/null
replaceinfile ${DISTSRC} ${DISTDST} "##VERSION##" ${VER_STR}

echo "Distribution ..."
${PB} ${PBOPT} ${PBRES} ${PBPKG} ${PACKAGEPATH}
if [ -e ${PACKAGEPATH} ];then
# this icon applying not affectible, need to find another way.
#    echo "read 'icns' (-16455) \"res/MIOHDR.app/Contents/Resources/AppIcon.icns\";"|Rez -o ${PACKAGEPATH};SetFile -a C ${PACKAGEPATH} 
    rm -rf ${POUTPATH}
fi
echo "done."
