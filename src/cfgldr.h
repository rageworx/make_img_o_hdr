#ifndef __CONFIGLOADER_H__
#define __CONFIGLOADER_H__

class ConfigLoader
{
    public:
        ConfigLoader( const char* argv0 = NULL );
        virtual ~ConfigLoader();

    public:
        bool Load();
        bool Reload();
        bool Save();

    // properties ---
    public: 
        void GetWindowPos( int &x, int &y, int &w, int &h, int &dt );
        void SetWindowPos( int x, int y, int w, int h, int dt );
        const char* GetLastPath();
        void SetLastPath( const char* path );
        unsigned GetLastMode();
        void SetLastMode( unsigned m );
        void GetLastParams( unsigned m, float* p );
        void SetLastParams( unsigned m, float* p );
        void GetResizeCopyFlag( unsigned &rsc ) { rsc = rszcpy; }
        void SetResizeCopyFlag( unsigned rsc ) { rszcpy = rsc; }

    protected:
        void clearall();
        void getbasepath();

    protected:
        bool        loaded;
        int         wx;
        int         wy;
        int         ww;
        int         wh;
        int         dt;
        unsigned    rszcpy;
        unsigned    p_m;
        float       p0_p[2];
        float       p1_p[4];
        float       p2_p[3];
        char        lastpath[512];

    protected:
        const char* pargv0;
        char        basepath[512];
};

#endif /// of __CONFIGLOADER_H__
