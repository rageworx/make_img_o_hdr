#include <sys/time.h>
#include <cmath>
#include <vector>
#include <algorithm>

#ifdef USE_OMP
#include <omp.h>
#endif /// of USE_OMP

#include <FL/Fl_Copy_Surface.H>
#include <FL/fl_draw.H>
#if defined(USE_SYSPNG)
	#include <png.h>
#else
	#include <FL/images/png.h>
#endif /// of defined(USE_SYSPNG)
#include "Fl_ImageViewer.H"
#include "fl_imgtk.h"

////////////////////////////////////////////////////////////////////////////////

#define MINIMAP_MAX_DISTANCE    150
#define MOVING_AMOUNT_DIVIDER   10
#define COLOR_NAVIBAR           0x9999FF00

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

unsigned max3us( unsigned i, unsigned j, unsigned k )
{
    unsigned maxv = i;
    if ( maxv < j ) maxv = j;
    if ( maxv < k ) maxv = k;

    return maxv;
}

template <typename T, size_t N> const T* vec_begin(const T (&a)[N]) { return a; }
template <typename T, size_t N> const T* vec_end  (const T (&a)[N]) { return a+N; }

////////////////////////////////////////////////////////////////////////////////

Fl_ImageViewer::Fl_ImageViewer(int x,int y,int w,int h)
 : Fl_Box( x, y, w, h ),
   multiplier( 1.0f ),
   imgsrc( NULL ),
   imgcached( NULL ),
   imgHistoSrc( NULL ),
   _notifier( NULL ),
   drawnavigator( false ),
   holddraw( false ),
   resize_type( 0 ),
   fittype( 1 ),
   naviMin( 0 ),
   naviMax( 1 ),
   naviCur( 0 ),
   naviTimestamp( 0 ),
   naviTimecheck( 0 ),
   naviBarCol( COLOR_NAVIBAR ),
   isdrawing( false ),
   isgeneratingimg( false )
{
    Fl_Box::box( FL_NO_BOX );
    color( FL_BLACK );
    type( 0 );

    labelcolor( FL_WHITE );

    naviTimecheck = gettimenow();
    naviTimestamp = naviTimecheck;
}

Fl_ImageViewer::~Fl_ImageViewer()
{
    unloadimage();
}

void Fl_ImageViewer::image(Fl_RGB_Image* aimg, int fittingtype)
{
    if( aimg == NULL )
        return;

    holddraw = true;

    unloadimage();

    imgsrc = (Fl_RGB_Image*)aimg->copy( aimg->w(), aimg->h() );

    holddraw = false;

    if ( imgsrc != NULL )
    {
        label( NULL );

        switch( fittingtype )
        {
            default:
            case 0:
                fittype = 0;
                fitwidth();
                break;

            case 1:
                fittype = 1;
                fitheight();
                break;

        }
    }
}

void Fl_ImageViewer::unloadimage()
{
    fl_imgtk::discard_user_rgb_image( imgcached );
    fl_imgtk::discard_user_rgb_image( imgsrc );
    fl_imgtk::discard_user_rgb_image( imgHistoSrc );
}

void Fl_ImageViewer::multiplyratio( float rf )
{
    if ( imgsrc == NULL )
        return;

    if ( isgeneratingimg == true )
        return;

    isgeneratingimg = true;

    bool  recalc_center = false;
    float rc_x = 0.0f;
    float rc_y = 0.0f;

    if ( imgcached != NULL )
    {
        Fl_RGB_Image* refimg = (Fl_RGB_Image*)imgcached;

        if ( ( refimg->array != NULL ) && ( refimg->alloc_array == 0 ) )
        {
            delete[] refimg->array;
        }

        delete imgcached;

        imgcached = NULL;
    }

    multiplier   = rf;
    float f_w    = imgsrc->w() * multiplier;
    float f_h    = imgsrc->h() * multiplier;

    imgcached = fl_imgtk::rescale( (Fl_RGB_Image*)imgsrc, f_w, f_h,
                                   (fl_imgtk::rescaletype)resize_type );

    isgeneratingimg = false;
}

unsigned long long Fl_ImageViewer::gettimenow()
{
    timeval tv;
    gettimeofday(&tv, NULL);
    return (unsigned long long)(tv.tv_sec * 1000 + tv.tv_usec / 1000);
}

void Fl_ImageViewer::fitwidth()
{
    if ( imgsrc == NULL )
        return;

    float f_w = float( w() ) / float( imgsrc->w() );

    multiplyratio( f_w );
}

void Fl_ImageViewer::fitheight()
{
    if ( imgsrc == NULL )
        return;

    float f_h = float( h() ) / float( imgsrc->h() );

    multiplyratio( f_h );
}

int Fl_ImageViewer::imgw()
{
    if ( imgcached != NULL )
    {
        return imgcached->w();
    }

    return 0;
}

int Fl_ImageViewer::imgh()
{
    if ( imgcached != NULL )
    {
        return imgcached->h();
    }

    return 0;
}

void Fl_ImageViewer::range( unsigned minv, unsigned maxv )
{
    if ( maxv > minv )
    {
        naviMin = minv;
        naviMax = maxv;

        if ( naviCur > naviMax )
        {
            naviCur = naviMax;
        }

        drawnavigator = true;
    }
    else
    {
        drawnavigator = false;
    }
}

void Fl_ImageViewer::position( unsigned v )
{
    if ( naviMax >= v )
    {
        naviCur = v;
    }
    else
    {
        naviCur = naviMax;
    }
}

int Fl_ImageViewer::handle( int event )
{
    int ret = Fl_Box::handle( event );

    static int  mouse_btn;
    static int  shift_key;
    static int  check_x;
    static int  check_y;

    switch( event )
    {
        case FL_DND_ENTER:
        case FL_DND_DRAG:
        case FL_DND_RELEASE:
        case FL_DND_LEAVE:
            if ( _notifier != NULL )
            {
                ret = 1;
            }
            else
            {
                ret = 0;
            }
            break;

        case FL_PASTE:
            if ( _notifier != NULL )
            {
                //static const char* retC = Fl::event_text();
                char* retC = strdup( Fl::event_text() );
                if ( retC != NULL )
                {
                    _notifier->OnDropFiles( this, retC );
                    free ( retC );
                }
                ret = 1;
            }
            else
            {
                ret = 0;
            }
            break;

        case FL_PUSH:
            take_focus();
            shift_key = Fl::event_shift();
            mouse_btn = Fl::event_button();
            //return 1;
            break;

        case FL_MOVE:
            check_x = Fl::event_x();
            check_y = Fl::event_y();

            if ( _notifier != NULL )
            {
                _notifier->OnMouseMove( this,
                                        Fl::event_x() - x(),
                                        Fl::event_y() - y(),
                                        true );
            }
            //return 1;
            break;

        case FL_LEAVE:
            if ( _notifier != NULL )
            {
                _notifier->OnMouseMove( this, 0, 0, false );
            }
            break;

        case FL_RELEASE:
            shift_key = Fl::event_shift();
            mouse_btn = Fl::event_button();

            if ( imgsrc != NULL )
            {
                check_x = Fl::event_x() - x();
                check_y = Fl::event_y() - y();

                if ( check_x < 0 )
                {
                    check_x = 0;
                }

                if ( check_y < 0 )
                {
                    check_y = 0;
                }

                if ( _notifier != NULL )
                {
                    _notifier->OnMouseClick( this,
                                             check_x,
                                             check_y,
                                             (unsigned)mouse_btn );
                }
            }
            //return 1;
            break;

        case FL_MOUSEWHEEL:
            if ( _notifier != NULL )
            {
                _notifier->OnKeyPressed( this,
                                         FL_MOUSEWHEEL,
                                         Fl::event_dx(),
                                         Fl::event_dy(),
                                         0 );
            }
            break;

        //case FL_KEYUP:
        case FL_KEYDOWN:
            {
                bool bredraw = false;

                int kbda = Fl::event_alt();
                int kbdc = Fl::event_ctrl();
				int kbds = Fl::event_shift();
                int kbdk = Fl::event_key();

#ifdef __APPLE__
				if ( kbdc == 0 )
				{
					kbdc = Fl::event_command();
				}
#endif /// of __APPLE__
                if ( _notifier != NULL )
                {
                    unsigned short kb = kbdk & 0xFFFF;

                    _notifier->OnKeyPressed( this, kb, kbds, kbdc, kbda );
                }

                ret = 1;
            }
            break;
    }

    return ret;
}

void Fl_ImageViewer::draw()
{
    if ( holddraw == true )
        return;

    if ( isdrawing == true )
        return;

    isdrawing = true;

    Fl::lock();

    fl_push_clip( x(), y(), w(), h() );

    Fl_Color prevC = fl_color();

    // Clear Background.
    fl_color( color() );
    fl_rectf( x(), y(), w(), h() );

    if ( ( imgcached != NULL ) && ( isgeneratingimg == false ) )
    {
        int put_x = x() + ( w() - imgcached->w() ) / 2;
        int put_y = y() + ( h() - imgcached->h() ) / 2;

        imgcached->draw( put_x, put_y );
    }
#ifdef DEBUG
    else
    {
        fl_font( FL_COURIER, 12 );
        fl_color( FL_RED );

        char outstr[128] = {0};

        snprintf( outstr, 128,
                 "No image cached, imgcached = %X, %d",
                 imgcached,
                 isgeneratingimg );

        fl_draw( outstr, x() + 30, y() + 30 );
    }
#endif // DEBUG

    // Draw Navigator bar
    if ( drawnavigator == true )
    {
        int nav_amount = naviMax - naviMin;
        int nav_cpos   = naviCur - naviMin;

        int nav_w_m = w() / 256;
        int nav_w   = w() / ( nav_amount + 1 );
        int nav_h   = h() / 256;

        if ( nav_w < 2 )
            nav_w = 2;

        if ( nav_h < 2 )
            nav_h = 2;

        int nav_x = x() + ( nav_w * nav_cpos ) + ( nav_w_m / 2 );
        int nav_y = y() + ( h() - ( h() / 100 ) );

        fl_color( FL_BLACK );
        fl_rectf( nav_x-1, nav_y-1, nav_w+2, nav_h+2 );
        fl_color( naviBarCol );
        fl_rectf( nav_x, nav_y, nav_w, nav_h );
        fl_font( FL_COURIER, 11 );

        static char posinfostr[32] = {0};
        snprintf( posinfostr, 32, "%d/%d", naviCur, naviMax );

        int put_x = nav_x;
        int put_y = nav_y - nav_h;

        fl_color( FL_BLACK );
        fl_draw( posinfostr, put_x-1, put_y-1 );
        fl_draw( posinfostr, put_x, put_y-1 );
        fl_draw( posinfostr, put_x+1, put_y-1 );
        fl_draw( posinfostr, put_x-1, put_y );
        fl_draw( posinfostr, put_x+1, put_y );
        fl_draw( posinfostr, put_x-1, put_y+1 );
        fl_draw( posinfostr, put_x, put_y+1 );
        fl_draw( posinfostr, put_x+1, put_y+1 );
        fl_color( naviBarCol );
        fl_draw( posinfostr, put_x, put_y );
    }

    // Draw Histogram.
    if ( imgHistoSrc != NULL )
    {
        imgHistoSrc->draw( 0, h() - imgHistoSrc->h() );
    }

    // Draw itself rectangle.
    Fl_Box::draw();

    fl_color( prevC );

    fl_pop_clip();

    Fl::unlock();

    if ( _notifier != NULL )
    {
        _notifier->OnDrawCompleted();
    }

    isdrawing = false;
}

void Fl_ImageViewer::resize(int x,int y,int w,int h)
{
    Fl_Box::resize( x, y, w, h );

    switch( fittype )
    {
        case 0:
            fitwidth();
            break;

        case 1:
            fitheight();
            break;
    }

    redraw();
}

void Fl_ImageViewer::box(Fl_Boxtype new_box)
{
    // It always ignore user custom box type.
    //Fl_Scroll::box( box() );
    //Fl_Box::box( FL_FLAT_BOX );
}

void Fl_ImageViewer::type(uchar t)
{
    // It always ignore user type = NO scroll bars.
    // Fl_Scroll::type( type() );
    Fl_Box::type( 0 );
}

void Fl_ImageViewer::resizemethod( int t, bool autoapply )
{
    if ( t <= 3 )
    {
        if ( resize_type != t )
        {
            resize_type = t;

            if ( autoapply == true )
            {
                multiplyratio( multiplier );
            }
        }
    }
}

bool Fl_ImageViewer::savetomonopng( const char* fpath )
{
    if ( imgcached == NULL )
        return false;

    FILE* fp = fopen( fpath, "wb" );

    if ( fp == NULL )
        return false;

    png_structp png_ptr     = NULL;
    png_infop   info_ptr    = NULL;
    png_bytep   row         = NULL;

    png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
    if ( png_ptr != NULL )
    {
        info_ptr = png_create_info_struct( png_ptr );
        if ( info_ptr != NULL )
        {
            if ( setjmp( png_jmpbuf( (png_ptr) ) ) == 0 )
            {
                int mx = imgcached->w();
                int my = imgcached->h();
                int pd = imgcached->d();

                png_init_io( png_ptr, fp );
                png_set_IHDR( png_ptr,
                              info_ptr,
                              mx,
                              my,
                              8,
                              PNG_COLOR_TYPE_GRAY,
                              PNG_INTERLACE_NONE,
                              PNG_COMPRESSION_TYPE_BASE,
                              PNG_FILTER_TYPE_BASE);

                png_write_info( png_ptr, info_ptr );

                row = (png_bytep)malloc( imgcached->w() * sizeof( png_byte ) );
                if ( row != NULL )
                {
                    const char* buf = imgcached->data()[0];
                    int bque = 0;

                    for( int y=0; y<my; y++ )
                    {
                        for( int x=0; x<mx; x++ )
                        {
                            //int bque = y * mx  + ( x * pd );
                            row[ x ] = buf[ bque ];
                            bque += pd;
                        }

                        png_write_row( png_ptr, row );
                    }

                    png_write_end( png_ptr, NULL );

                    fclose( fp );

                    free(row);
                }

                png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

                return true;
            }
        }
    }

	return false;
}

void Fl_ImageViewer::updateHistogram()
{
    if ( imgsrc != NULL )
    {
        unsigned bufflen = imgsrc->w() * imgsrc->h();
        unsigned col_d = imgsrc->d();

        if ( bufflen == 0 )
            return;

        if ( col_d < 3 )
            return;

        const uchar* pbuff = (const uchar*)imgsrc->data()[0];

        vector< unsigned > amounts[3];

        for( unsigned cnt=0; cnt<3; cnt++ )
        {
            amounts[cnt].resize( 256 );
        }

        #pragma omp parallel for
        for( unsigned cnt=1; cnt<bufflen; cnt++ )
        {
            // Counting except 0 (black)
            // B->G->R(->A) ...
            amounts[ 0 ][ pbuff[ cnt * col_d + 0 ] ] ++;
            amounts[ 1 ][ pbuff[ cnt * col_d + 1 ] ] ++;
            amounts[ 2 ][ pbuff[ cnt * col_d + 2 ] ] ++;
        }

        unsigned maxintensity = 0;

        unsigned max_b = *max_element( amounts[0].begin(), amounts[0].end() );
        unsigned max_g = *max_element( amounts[1].begin(), amounts[1].end() );
        unsigned max_r = *max_element( amounts[2].begin(), amounts[2].end() );

        maxintensity = max3us( max_b, max_g, max_r );

        fl_imgtk::discard_user_rgb_image( imgHistoSrc );

        imgHistoSrc = fl_imgtk::makeanempty( 256, 100, 4, 0x0000007F );
        if ( imgHistoSrc != NULL )
        {
            uchar* refbuff = (uchar*)imgHistoSrc->data()[0];

            #pragma omp parallel for
            for( unsigned cnt=1; cnt<256; cnt++ )
            {
                // R - G - B 3 turns
                for( unsigned xx=0; xx<3; xx++ )
                {
                    for( unsigned yy=(maxintensity-amounts[xx][cnt]); yy<maxintensity; yy++ )
                    {
                        unsigned ypos = ( (float)100.f / (float)maxintensity ) * (float)yy;
                        refbuff[ ( ypos * 256 + cnt ) * 4  + xx ] = 0xFF;
                    }
                }
            }

            // Make black to transparency.
            uchar* buff = (uchar*)imgHistoSrc->data()[0];
            unsigned buffsz = imgHistoSrc->w() * imgHistoSrc->h();

            fl_imgtk::draw_rect( imgHistoSrc, 
                                 0, 0, 
                                 imgHistoSrc->w()-1, imgHistoSrc->h()-1, 0x9F9F9FFF );
            
            fl_imgtk::draw_line( imgHistoSrc,
                                 1, imgHistoSrc->h() / 2 - 1,
                                 imgHistoSrc->w() - 2 - 2, imgHistoSrc->h() / 2 - 1,
                                 0x9F2F9F9F );

            #pragma omp parallel for
            for( unsigned cnt=0; cnt<buffsz; cnt++ )
            {
                unsigned avr = ( buff[ cnt * 4 + 0 ] \
                               + buff[ cnt * 4 + 1 ] \
                               + buff[ cnt * 4 + 2 ] ) / 3;
                if ( avr == 0 )
                {
                    buff[ cnt * 4 + 3 ] = 0x20;
                }
            }

            imgHistoSrc->uncache();
        }

        // discards used vectors.
        for( unsigned cnt=2; cnt==0; cnt-- )
        {
            vector< unsigned >().swap( amounts[cnt] );
        }
    }
}
