#ifndef __WINMAIN_H__
#define __WINMAIN_H__

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_SVG_Image.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Menu_.H>
#include <FL/Fl_Menu_Item.H>
#include <FL/Fl_Menu_Button.H>

#include "Fl_ImageViewer.H"
#include "Fl_Image_Button.h"
#include "Fl_TransBox.h"

#include <pthread.h>

#include <vector>
#include <string>

#include "cfgldr.h"

class WMain : public Fl_ImageViewerNotifier
{
    typedef struct
        _ThreadParam{
            pthread_t   ptt;
            long        paramL;
            void*       paramData;
        }ThreadParam;

    public:
        WMain( int argc, char** argv );
        ~WMain();

    public:
        int Run();

    public:
        void* PThreadCall( ThreadParam* tparam );
        static void* PThreadCB( void* p )
        {
            if ( p != NULL )
            {
                ThreadParam* tp = (ThreadParam*)p;
                WMain* pwm = (WMain*)tp->paramData;
                return pwm->PThreadCall(tp);
            }
            return NULL;
        }
        void WidgetCall( Fl_Widget* w );
        static void WidgetCB( Fl_Widget* w, void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                pwm->WidgetCall( w );
            }
        }
        void TimerCall();
        static void TimerCB( void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                return pwm->TimerCall();
            }
        }

        void PNGWriteCall(unsigned current, unsigned maxsz);
        static void PNGWriteCB(void* p, unsigned c, unsigned m)
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                pwm->PNGWriteCall(c,m);
            }
        }
    private:
        void setdefaultwintitle();
        void resetParameters();
        void createComponents();
        void registerPopMenu();
        void getEnvironments();
        void updateImageHistogram();
        void updateCrossFadeBG();
        // int  testImageFile();
        // result : <0 = unknown, 1=JPG, 2=PNG, 3=BMP ...
#ifdef SUPPORT_WCHAR
        int  testImageFile( const wchar_t* imgfp = NULL, char** buff = NULL, size_t* buffsz = NULL );
#else
        int  testImageFile( const char* imgfp = NULL, char** buff = NULL, size_t* buffsz = NULL );
#endif
        void loadImage();
        int  sensFitting( const Fl_RGB_Image* img );
        bool saveImageProc();
        bool copy2clipboard( int cflag = 0 );
        void rotateLeft();
        void rotateRight();
        bool createThread( unsigned idx );
        void killThread( unsigned idx );

    private:
        int     _argc;
        char**  _argv;

    private:
        float       crossfade_ratio;
        unsigned    crossfade_idx;
        bool        timermutex;
        bool        busyflag;

    protected: /// inherit to Fl_ImageViewerNotifier
        void OnMouseMove( void* w, int x, int y, bool inside );
        void OnMouseClick( void* w, int x, int y, unsigned btn );
        void OnKeyPressed( void* w, unsigned short k, int s, int c, int a );
        void OnDropFiles( void* w, const char* files );
        void OnDrawCompleted();

    protected:
        Fl_Double_Window*   window;
        Fl_Group*           grpControl;
        Fl_Group*           grpControlActive;
        Fl_Group*           grpControlEmpty;
        Fl_Group*           grpControlTool;
#if defined(DISABLE_BLURBACK)
        Fl_TransBox*        boxControl;
#else
        Fl_Box*             boxControl;
#endif /// of defined(DISABLE_BLURBACK)
        Fl_Group*           grpImageView;
        Fl_ImageViewer*     boxImgPreView;
        Fl_RGB_Image*       imgControlBG;
        Fl_RGB_Image*       imgSource;
        Fl_RGB_Image*       imgHDRed;

        Fl_Choice*          chsHDR;
        Fl_Input*           inpParam[4];
        Fl_Button*          btnReset;
        Fl_Image_Button*    btnLoad;
        Fl_Image_Button*    btnRotLeft;
        Fl_Image_Button*    btnRotRight;
        Fl_Image_Button*    btnApply;
        Fl_Image_Button*    btnCopy2Clip;
        Fl_Image_Button*    btnSaveTo;
        Fl_Image_Button*    btnNextImg;
        Fl_Image_Button*    btnPrevImg;
        Fl_TransBox*        boxProgress;

        Fl_Menu_Button*     popMenu;

    private:
        #define             MAX_WINTITLE_LEN        512
        char wintitlestr[MAX_WINTITLE_LEN];

    protected:
#ifdef SUPPORT_WCHAR
        std::wstring        pathHome;
        std::wstring        pathSystemBase;
        std::wstring        pathUserData;
        std::wstring        pathUserRoaming;
#else
        std::string         pathHome;
        std::string         pathSystemBase;
        std::string         pathUserData;
        std::string         pathUserRoaming;
#endif

    protected:
#ifdef SUPPORT_WCHAR
        std::wstring        pathImage;
        std::wstring        imgFName;
#else
        std::string         pathImage;
        std::string         imgFName;
#endif
        std::string         imgFNameUTF8;
        std::string         strProgress;
        std::string         savefn;

    protected:
        int                 timerEventID;
        bool                tmutex;
        int                 tstate;

    protected:
        #define             MAX_THREADS_CNT             3
        #define             THREAD_KILL_ALL             -1
        #define             THREAD_JOB_APPLYING_HDR     0
        #define             THREAD_JOB_UPDATE_HISTO     1
        #define             THREAD_JOB_SAVE_FILE        2
        ThreadParam*        threads[MAX_THREADS_CNT];
        ConfigLoader*       cfgLdr;
};

#endif // __WINMAIN_H__
