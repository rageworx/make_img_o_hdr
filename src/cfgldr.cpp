#include <unistd.h>

#ifndef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#endif

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

#include <FL/fl_utf8.h>
#include <minIni.h>
#include "cfgldr.h"

///////////////////////////////////////////////////////////////////////

using namespace std;

///////////////////////////////////////////////////////////////////////

#define CFGF_NAME   "miohdr.cfg"
#define DEFAULT_INI "[WINDOW]\nX=20\nY=20\nW=790\nH=440\nDT=0\n"\
                    "[USER]\nlastmode=0\nlastpath=\nresizecopy=1\n"

///////////////////////////////////////////////////////////////////////

void createdefaultini( string &fpath )
{
    FILE* fp = fopen( fpath.c_str(), "wb" );
    if ( fp != NULL )
    {
        char refstr[] = DEFAULT_INI;
        fwrite( refstr, 1, strlen( refstr ), fp );
        fclose( fp );
    }
    else
    {
        fprintf( stderr, "Failed to create INI !\n%s\n", fpath.c_str() );
    }
}

///////////////////////////////////////////////////////////////////////

ConfigLoader::ConfigLoader( const char* argv0 )
    : pargv0( argv0 )
{
    clearall();
    getbasepath();
}

ConfigLoader::~ConfigLoader()
{
    Save();
}

bool ConfigLoader::Load()
{
    loaded = false;

    string loadpath = basepath;

    if ( loadpath.size() == 0 )
    {
        loadpath = ".";
    }

    // check directory first.
    if ( access( loadpath.c_str(), 0 ) != 0 )
    {
#if defined(_WIN32)
        mkdir( loadpath.c_str() );
#else
        mkdir( loadpath.c_str(), 0775 );
#endif      
    }

#if defined(_WIN32)
    loadpath += "\\";
#else
    loadpath += "/";
#endif /// of defined(_WIN32)
    loadpath += CFGF_NAME;

    if ( access( loadpath.c_str(), 0 ) != 0 )
    {
        createdefaultini( loadpath );
    }

#ifdef DEBUG
    printf( "Starting to load config in %s ...\n", loadpath.c_str() );
    fflush( stdout );
#endif 
    minIni *pIni = new minIni( loadpath );
    if ( pIni != NULL )
    {
        wx = pIni->geti( "WINDOW", "X", 10 );
        wy = pIni->geti( "WINDOW", "Y", 10 );
        ww = pIni->geti( "WINDOW", "W", 790 );
        wh = pIni->geti( "WINDOW", "H", 440 );
        dt = pIni->geti( "WINDOW", "DT", 0 );

        string tmpstr;
        p_m    = (unsigned)pIni->geti( "USER", "lastmode", 0 );
        rszcpy = (unsigned)pIni->geti( "USER", "resizecopy", 1 );
        tmpstr = pIni->gets( "USER", "lastpath", "." );
        if ( tmpstr.size() > 0 )
        {
            strncpy( lastpath, tmpstr.c_str(), 512 );
        }

        for( int cnt=0; cnt<3; cnt++ )
        {
            char tmpmap[32] = {0};
            snprintf( tmpmap, 32, "p%d_p", cnt );

            switch( cnt )
            {
                case 0: /// log mapping : 2 params
                    p0_p[0] = pIni->getf( tmpmap, "p1", 1.0 );
                    p0_p[1] = pIni->getf( tmpmap, "p2", 0.0 );
                    break;

                case 1: /// reinhard : 4 params
                    p1_p[0] = pIni->getf( tmpmap, "p1", 1.0 );
                    p1_p[1] = pIni->getf( tmpmap, "p2", 0.3 );
                    p1_p[2] = pIni->getf( tmpmap, "p3", 0.5 );
                    p1_p[3] = pIni->getf( tmpmap, "p4", 0.0 );
                    break;

                case 2: /// CLAHE : 3 params
                    p2_p[0] = pIni->getf( tmpmap, "p1", 2.0 );
                    p2_p[1] = pIni->getf( tmpmap, "p2", 2.0 );
                    p2_p[2] = pIni->getf( tmpmap, "p3", 1.9 );
                    break;
            }
        }

        delete pIni;

        loaded = true;
    }

    return loaded;
}

bool ConfigLoader::Reload()
{
    clearall();
    return Load();
}

bool ConfigLoader::Save()
{
    if ( loaded == true )
    {
        string loadpath = basepath;

        if ( loadpath.size() == 0 )
        {
            loadpath = ".";
        }

#if defined(_WIN32)
        loadpath += "\\";
#else
        loadpath += "/";
#endif
        loadpath += CFGF_NAME;

        minIni *pIni = new minIni( loadpath );
        if ( pIni != NULL )
        {
            pIni->put( "WINDOW", "X", wx );
            pIni->put( "WINDOW", "Y", wy );
            pIni->put( "WINDOW", "W", ww );
            pIni->put( "WINDOW", "H", wh );
            pIni->put( "WINDOW", "DT", dt );

            string tmpstr;
            pIni->put( "USER", "lastmode", (int)p_m );
            pIni->put( "USER", "lastpath", lastpath );
            pIni->put( "USER", "resizecopy", (int)rszcpy );

            for( int cnt=0; cnt<3; cnt++ )
            {
                char tmpmap[32] = {0};
                snprintf( tmpmap, 32, "p%d_p", cnt );

                switch( cnt )
                {
                    case 0: /// log mapping : 2 params
                        pIni->put( tmpmap, "p1", p0_p[0] );
                        pIni->put( tmpmap, "p2", p0_p[1] );
                        break;

                    case 1: /// reinhard : 4 params
                        pIni->put( tmpmap, "p1", p1_p[0] );
                        pIni->put( tmpmap, "p2", p1_p[1] );
                        pIni->put( tmpmap, "p3", p1_p[2] );
                        pIni->put( tmpmap, "p4", p1_p[3] );
                        break;

                    case 2: /// CLAHE : 3 params
                        pIni->put( tmpmap, "p1", p2_p[0] );
                        pIni->put( tmpmap, "p2", p2_p[1] );
                        pIni->put( tmpmap, "p3", p2_p[2] );
                        break;
                }
            }

            delete pIni;

            return true;
        }
    }
    return false;
}

void ConfigLoader::GetWindowPos( int &x, int &y, int &w, int &h, int &dt )
{
    x = wx;
    y = wy;
    w = ww;
    h = wh;
    dt = this->dt;
}

void ConfigLoader::SetWindowPos( int x, int y, int w, int h, int dt )
{
    wx = x;
    wy = y;
    ww = w;
    wh = h;
    this->dt = dt;
}

const char* ConfigLoader::GetLastPath()
{
    return lastpath;
}

void ConfigLoader::SetLastPath( const char* path )
{
    strncpy( lastpath, path, 512 );
}

unsigned ConfigLoader::GetLastMode()
{
    return p_m;
}

void ConfigLoader::SetLastMode( unsigned m )
{
    p_m = m;
}

void ConfigLoader::GetLastParams( unsigned m, float* p )
{
    if ( p == NULL )
        return;

    switch( m )
    {
        case 0:
            memcpy( p, p0_p, sizeof(float)*2 );
            break;

        case 1:
            memcpy( p, p1_p, sizeof(float)*4 );
            break;

        case 2:
            memcpy( p, p2_p, sizeof(float)*3 );
            break;
    }
}

void ConfigLoader::SetLastParams( unsigned m, float* p )
{
    p_m = m;
    
    if ( p == NULL )
        return;

    switch( m )
    {
        case 0:
            memcpy( p0_p, p, sizeof(float)*2 );
            break;

        case 1:
            memcpy( p1_p, p, sizeof(float)*4 );
            break;

        case 2:
            memcpy( p2_p, p, sizeof(float)*3 );
            break;
    }
}

void ConfigLoader::clearall()
{
    p_m = 0;
    memset( p0_p, 0, sizeof(float)*2 );
    memset( p1_p, 0, sizeof(float)*4 );
    memset( p2_p, 0, sizeof(float)*3 );
    memset( lastpath, 0 ,512 );
    memset( basepath, 0 ,512 );
}

void ConfigLoader::getbasepath()
{
#if defined(_WIN32)
    const wchar_t* ppath = _wgetenv( L"LOCALAPPDATA" );
    if ( ppath != NULL )
    {
        // convert to damn wchar_t to utf8.
        char convutf8[512] = {0};
        unsigned clen = fl_utf8fromwc( convutf8,
                                       512,
                                       ppath,
                                       wcslen( ppath ) );
        if ( clen > 0 )
        {
            memset( basepath, 0, 512 );
            strcpy( basepath, convutf8 );
            strcat( basepath, "\\MIOHDR" );
        }
    }
#elif defined(__APPLE__)
    const char* ppath = getenv( "HOME" );
    if ( ppath != NULL )
    {
        memset( basepath, 0, 512 );
        strcpy( basepath, ppath );
        strcat( basepath, "/Library/Application Support/MIOHDR" );
    }
#else
    const char* ppath = getenv( "HOME" );
    if ( ppath != NULL )
    {
        memset( basepath, 0, 512 );
        strcpy( basepath, ppath );
        strcat( basepath, "/.MIOHDR" );
    }
#endif
}
