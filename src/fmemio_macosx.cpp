#ifdef __APPLE__
#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <string>

#ifdef __APPLE__
    #include <sys/uio.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <sys/mman.h>
#else
    #include <io.h>
#endif /// of __APPLE__

#ifndef uchar
#   define uchar    unsigned char
#endif

typedef struct fmem_t 
{
    size_t  pos;
    size_t  size;
    uchar*  buffer;
}fmem;

static int readfn(void* handler, char* buf, int size) 
{
    if ( ( handler == NULL ) || ( buf == NULL ) )
        return 0;
        
    fmem_t* mem = (fmem_t*)handler;
    size_t  available = mem->size - mem->pos;

    if (size > available) 
    {
        size = available;
    }
    
    memcpy( buf, 
            mem->buffer + mem->pos, 
            sizeof(uchar) * size );
    
    mem->pos += size;

    return size;
}

static int writefn(void* handler, const char* buf, int size) 
{
    if ( handler == NULL )
        return 0;

    fmem_t* mem       = (fmem_t*)handler;
    size_t  available = mem->size - mem->pos;

    if (size > available) 
    {
        size = available;
    }
    
    memcpy( mem->buffer + mem->pos, 
            buf, 
            sizeof(uchar) * size );
            
    mem->pos += size;

    return size;
}

static fpos_t seekfn(void* handler, fpos_t offset, int wh) 
{
    size_t  pos;
    fmem_t* mem = (fmem_t*)handler;

    switch ( wh ) 
    {
        case SEEK_SET: 
            {
                if ( offset >= 0 ) 
                {
                    pos = (size_t)offset;
                } 
                else 
                {
                    pos = 0;
                }
            }
            break;
        
        case SEEK_CUR: 
            {
                if ( ( offset >= 0 ) || 
                     ( (size_t)(-offset) <= mem->pos ) ) 
                {
                    pos = mem->pos + (size_t)offset;
                } 
                else 
                {
                    pos = 0;
                }
            }
            break;
        
        case SEEK_END: 
            {
                pos = mem->size + (size_t)offset;
            }
            break;
            
        default: 
            return -1;
    }

    if ( pos > mem->size ) 
    {
        return -1;
    }

    mem->pos = pos;
    
    return (fpos_t)pos;
}

static int closefn(void *handler) 
{
    if ( handler != NULL )
    {
        delete[] (fmem_t*)handler;
        handler = NULL;
    }
    
    return 0;
}

FILE* fmemopen(void *buf, size_t size, const char *mode) 
{
    if ( ( buf = NULL ) || ( size <= 0 ) )
        return NULL;
    
    fmem_t* mem = new fmem_t;

    memset( mem, 0, sizeof(fmem_t) );

    mem->size = size;
    mem->buffer = (uchar*)buf;

    return funopen(mem, readfn, writefn, seekfn, closefn);
}
#endif /// of __APPLE__
