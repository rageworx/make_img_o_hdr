#ifdef _WIN32

#include <Fl/fl_draw.h>
#include <windows.h>
#include <omp.h>
#include "platformclipboard.h"

HBITMAP CreateBitmapFromFlRGB( const Fl_RGB_Image* img, unsigned char* &ptr )
{
    if ( img != NULL )
    {
        int bufflen = img->w() * img->h() * img->d();
        if ( ( bufflen > 0 ) && ( img->d() >=3 ) )
        {
            int bmplen = img->w() * img->h() * 3;
            unsigned char* buffref = (unsigned char*)img->data()[0];
            unsigned char* buffcp = new unsigned char[ bmplen ];
            
            if ( buffcp == NULL )
                return 0;
            
            // convert Fl_RGB to windows BGR ...
            int img_d = img->d();
            int img_wxh = img->w() * img->h();
                         
            BITMAPINFO bmi = { sizeof(BITMAPINFOHEADER), 
                               img->w(), 
                               -(img->h()), 
                               1, 
                               24,
                               BI_RGB, 
                               0, 0, 0, 0, 0 };
            HDC hDC = CreateCompatibleDC( GetDC( 0 ) );
            HBITMAP hBmp = CreateDIBSection( hDC, &bmi, DIB_RGB_COLORS, (void**)&buffcp, 0,0 );
 
            #pragma omp parallel for
            for( int cnt=0; cnt<img_wxh; cnt++ )
            {
                buffcp[cnt*3 + 0] = buffref[cnt*img_d + 2];
                buffcp[cnt*3 + 1] = buffref[cnt*img_d + 1];
                buffcp[cnt*3 + 2] = buffref[cnt*img_d + 0];
            }
 
            // don't delete[] buffcp;
            DeleteDC( hDC );
            ptr = buffcp;
            return hBmp;
        }
    }
    
    return 0;
}

bool BitmapToClipboard( HBITMAP hBMP, HWND hWnd = NULL )
{
    if ( OpenClipboard(hWnd) == FALSE )
        return false;

    EmptyClipboard();

    BITMAP bm;
    GetObject( hBMP, sizeof(bm), &bm );

    BITMAPINFOHEADER bi;
    ZeroMemory(&bi, sizeof(BITMAPINFOHEADER));
    bi.biSize = sizeof(BITMAPINFOHEADER);
    bi.biWidth = bm.bmWidth;
    bi.biHeight = bm.bmHeight;
    bi.biPlanes = 1;
    bi.biBitCount = bm.bmBitsPixel;
    bi.biCompression = BI_RGB;

    if (bi.biBitCount <= 1)	// make sure bits per pixel is valid
        bi.biBitCount = 1;
    else if (bi.biBitCount <= 4)
        bi.biBitCount = 4;
    else if (bi.biBitCount <= 8)
        bi.biBitCount = 8;
    else // if greater than 8-bit, force to 24-bit
        bi.biBitCount = 24;

    // Get size of color table.
    SIZE_T dwColTableLen = (bi.biBitCount <= 8) ? (1 << bi.biBitCount) * sizeof(RGBQUAD) : 0;
    // Create a device context with palette
    HDC hDC = GetDC(NULL);
    HPALETTE hPal = static_cast<HPALETTE>(::GetStockObject(DEFAULT_PALETTE));
    HPALETTE hOldPal = ::SelectPalette(hDC, hPal, FALSE);
    RealizePalette(hDC);

    GetDIBits( hDC, hBMP, 0, bi.biHeight, NULL,
               (LPBITMAPINFO)&bi, DIB_RGB_COLORS);

    if ( bi.biSizeImage == 0 )
    {
        bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) * bi.biHeight;
    }

    HGLOBAL hDIB = GlobalAlloc( GMEM_MOVEABLE,
                                sizeof(BITMAPINFOHEADER) + dwColTableLen + bi.biSizeImage );
    if ( hDIB != NULL )
    {
        union tagHdr_u
        {
            LPVOID             p;
            LPBYTE             pByte;
            LPBITMAPINFOHEADER pHdr;
            LPBITMAPINFO       pInfo;
        } Hdr;

        Hdr.p = ::GlobalLock(hDIB);

        CopyMemory(Hdr.p, &bi, sizeof(BITMAPINFOHEADER));
        int nConv = GetDIBits( hDC, hBMP, 0, bi.biHeight,
                               Hdr.pByte + sizeof(BITMAPINFOHEADER) + dwColTableLen,
                               Hdr.pInfo, DIB_RGB_COLORS );
        GlobalUnlock(hDIB);
        if (!nConv)
        {
            GlobalFree(hDIB);
            hDIB = NULL;
        }
    }

    if (hDIB)
    {
        SetClipboardData(CF_DIB, hDIB);
    }

    CloseClipboard();
    SelectPalette(hDC, hOldPal, FALSE);
    ReleaseDC(NULL, hDC);

    return true;
}

bool CopyImageToClipboard( Fl_RGB_Image* img )
{
    if ( img != NULL )
    {
        unsigned char* pdata = NULL;
        HBITMAP retbmp = CreateBitmapFromFlRGB( img, pdata );
        if ( retbmp != 0 )
        {
            bool retb = BitmapToClipboard( retbmp );
            DeleteObject( retbmp );
            return retb;
        }
    }
    return false;
}

#else

#include <FL/fl_draw.H>
#include "platformclipboard.h"
#include <FL/Fl_Copy_Surface.H>

bool CopyImageToClipboard( Fl_RGB_Image* img )
{
    if ( img != NULL )
    {
        Fl_Copy_Surface* copy_surf = new Fl_Copy_Surface( img->w(), img->h() );
        if ( copy_surf != NULL )
        {                
            copy_surf->set_current();   /// direct graphics requests to the clipboard
            fl_color( 0x00000000 );     /// transparent empty space
            fl_rectf( 0, 0, copy_surf->w(), copy_surf->h());

            img->draw( 0, 0 , img->w(), img->h() );

            // after this, the clipboard is loaded
            delete copy_surf;
            // direct graphics requests back to the display
            Fl_Display_Device::display_device()->set_current();

            return true;
        }
    }
    return false;
}
#endif /// of _WIN32
