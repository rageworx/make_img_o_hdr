#ifndef FL_IMAGE_BUTTON_H
#define FL_IMAGE_BUTTON_H

#include <FL/Fl_Button.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_SVG_Image.H>
#include <FL/Fl_RGB_Image.H>

class Fl_Image_Button
 : public Fl_Button
{
    public:
        Fl_Image_Button(int X, int Y, int W, int H, const char* L=0 );
        virtual ~Fl_Image_Button();

    public:
        void      image( Fl_SVG_Image* img );
        Fl_Image* image() { return activateImage; }

    protected:
        void draw();

    protected:
        virtual int handle(int event);

    private:
        bool hovered;

    private:
        Fl_Image*   activateImage;
        Fl_Image*   pushImage;
        Fl_Image*   deactivateImage;
        Fl_Image*   hoverImage;
        Fl_Image*   currentImage;
};

#endif // FL_IMAGE_BUTTON_H
