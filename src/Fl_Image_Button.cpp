#include <FL/fl_draw.H>
#include "Fl_Image_Button.h"

Fl_Image_Button::Fl_Image_Button(int X, int Y, int W, int H, const char* L )
 : Fl_Button(X, Y, W, H, L),
   activateImage(NULL),
   pushImage(NULL),
   deactivateImage(NULL),
   currentImage(NULL)
{
    box(FL_NO_BOX);
    align(FL_ALIGN_IMAGE_BACKDROP);
    clear_visible_focus();
}

Fl_Image_Button::~Fl_Image_Button()
{
    delete activateImage;
    delete pushImage;
    delete deactivateImage;
}

void Fl_Image_Button::image( Fl_SVG_Image* img )
{
    delete activateImage;
    delete pushImage;
    delete deactivateImage;

    if ( img != NULL )
    {
        img->scale( w(), h() );
        
        activateImage = img;     
        pushImage = (Fl_Image*)img->copy( w(), h() );
        if ( pushImage != NULL )
            pushImage->color_average( 0, 0.3f );
        
        deactivateImage = (Fl_Image*)img->copy( w(), h() );
        if ( deactivateImage != NULL )
            deactivateImage->color_average( 0, 0.3f );

        currentImage = activateImage;
    }
    else
    {
        currentImage = NULL;
    }
}

int Fl_Image_Button::handle(int event)
{
    switch ( event )
    {

        case FL_PUSH:
            if ( pushImage != NULL )
            {
                currentImage = pushImage;
                damage( 0 );
            }
            break;

        case FL_RELEASE:
            if ( activateImage != NULL )
            {
                currentImage = activateImage;
                damage( 0 );
            }
            break;

        case FL_DEACTIVATE:
            if ( deactivateImage != NULL )
            {
                currentImage = deactivateImage;
                damage( 0 );
            }
            break;

        case FL_ACTIVATE:
            if ( activateImage != NULL )
            {
                currentImage = activateImage;
                damage( 0 );
            }
            break;
    }

    return (Fl_Button::handle(event));
}

void Fl_Image_Button::draw()
{
    fl_push_clip( x(), y(), w(), h() );

    if ( currentImage != NULL )
    {
        int c_x = ( w() - currentImage->w() ) / 2;
        int c_y = ( h() - currentImage->h() ) / 2;

        currentImage->draw( x() + c_x, y() + c_y );
    }

    fl_pop_clip();
}
