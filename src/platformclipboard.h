#ifndef __PLATFORMCLIPBOARD_H__
#define __PLATFORMCLIPBOARD_H__

#include <FL/Fl.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_RGB_Image.H>

bool CopyImageToClipboard( Fl_RGB_Image* img );

#endif // __PLATFORMCLIPBOARD_H__
