#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include "winMain.h"

#ifdef __linux__
    #include <signal.h>
#endif

#ifdef _WIN32
    #include <windows.h>
    #include <shellscalingapi.h>
#endif

#include <FL/platform.H>
#include <FL/Fl_Copy_Surface.H>
#include <FL/fl_draw.H>
#include <FL/fl_ask.H>
#if defined(USE_SYSPNG)
	#include <png.h>
#else
	#include <FL/images/png.h>
#endif /// of defined(USE_SYSPNG)
#include <fl_imgtk.h>

#include "resource.h"
#include "dtools.h"
#include "ltools.h"

#include <FL/Fl_BMP_Image.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_Native_File_Chooser.H>

#include "platformclipboard.h"
#include "fl_pngwriter.h"

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME            "Make Image O' HDR"
#define DEF_WIDGET_FSZ          12
#ifdef __linux__
#define DEF_WIDGET_FNT          FL_HELVETICA
#else
#define DEF_WIDGET_FNT          FL_FREE_FONT
#endif // __linux__

#define DEF_WIN_COLOR_BG        0x33333300
#define DEF_WIN_COLOR_FG        0xFFFFFF00

////////////////////////////////////////////////////////////////////////////////

static string   resourcebase;

////////////////////////////////////////////////////////////////////////////////

bool getResource( uint8_t rtype, const char* scheme, uchar** buff, unsigned* buffsz )
{
#ifdef _WIN32
    wchar_t convscheme[80] = {0,};

    fl_utf8towc( scheme, strlen(scheme), convscheme, 80 );

    HRSRC rsrc = FindResource( NULL, convscheme, RT_RCDATA );
    if ( rsrc != NULL )
    {
        *buffsz = SizeofResource( NULL, rsrc );
        if ( *buffsz > 0 )
        {
            HGLOBAL glb = LoadResource( NULL, rsrc );
            if ( glb != NULL )
            {
                void* fb = LockResource( glb );
                if ( fb != NULL )
                {
                    uchar* cpbuff = new uchar[ *buffsz ];
                    if ( cpbuff != NULL )
                    {
                        memcpy( cpbuff, fb, *buffsz );

                        *buff = cpbuff;

                        UnlockResource( glb );
                        return true;
                    }

                    UnlockResource( glb );
                }
            }
        }
    }
#elif defined(__APPLE__)
    string rescomb = resourcebase;
    rescomb += "/";
    rescomb += scheme;
    if ( rtype == 0 )
        rescomb += ".png";
    else
        rescomb += ".svg";
#ifdef DEBUG
    printf("Load resource : %s : ", rescomb.c_str() );
#endif
    FILE* fp = fopen( rescomb.c_str(), "rb" );
    if ( fp != NULL )
    {
        fseek( fp, 0L, SEEK_END );
        size_t flen = ftell( fp );
        fseek( fp, 0L, SEEK_SET );

        if ( flen > 0 )
        {
            uchar* cpbuff = new uchar[ flen ];
            if ( cpbuff != NULL )
            {
                fread( cpbuff, 1, flen, fp );
                fclose( fp );
                *buffsz = flen;
                *buff = cpbuff;
#ifdef DEBUG
                printf("Ok.\n");
#endif
                return true;
            }
        }
        else
        {
            fclose( fp );
        }
    }
#ifdef DEBUG
    printf("Failure.\n");
#endif

#else /// --> Linux
    string rescomb = resourcebase;
    rescomb += "/";
    rescomb += scheme;
    if ( rtype == 0 )
        rescomb += ".png";
    else
        rescomb += ".svg";
#ifdef DEBUG
    printf("Load resource : %s : ", rescomb.c_str() );
#endif
    FILE* fp = fopen( rescomb.c_str(), "rb" );
    if ( fp != NULL )
    {
        fseek( fp, 0L, SEEK_END );
        size_t flen = ftell( fp );
        fseek( fp, 0L, SEEK_SET );

        if ( flen > 0 )
        {
            uchar* cpbuff = new uchar[ flen ];
            if ( cpbuff != NULL )
            {
                fread( cpbuff, 1, flen, fp );
                fclose( fp );
                *buffsz = flen;
                *buff = cpbuff;
#ifdef DEBUG
                printf("Ok.\n");
#endif
                return true;
            }
        }
        else
        {
            fclose( fp );
        }
    }

#endif /// of _WIN32

    return false;
}

Fl_RGB_Image* createResImage( const char* scheme )
{
    if ( scheme != NULL )
    {
        uchar* buff = NULL;
        unsigned buffsz = 0;

        if ( getResource( 0, scheme, &buff, &buffsz ) == true )
        {
            Fl_RGB_Image* retimg = new Fl_PNG_Image( scheme, buff, buffsz );
            delete[] buff;

            return retimg;
        }
    }

    return NULL;
}

Fl_SVG_Image* createResSVGImage( const char* scheme )
{
    if ( scheme != NULL )
    {
        uchar* buff = NULL;
        unsigned buffsz = 0;

        if ( getResource( 1, scheme, &buff, &buffsz ) == true )
        {
            Fl_SVG_Image* retimg = new Fl_SVG_Image( scheme, buff, buffsz );
            delete[] buff;

            return retimg;
        }
    }

    return NULL;
}

#ifdef _WIN32
typedef int(__stdcall *MSGBOXWAPI)(IN HWND, IN LPCWSTR, IN LPCWSTR, IN UINT, IN WORD, IN DWORD);

int X_MessageBoxTimeout(HWND hWnd, const TCHAR* sText, const TCHAR* sCaption, UINT uType, DWORD dwMilliseconds)
{
    int retI = 0;

    HMODULE hUser32 = LoadLibraryA( "user32.dll" );

    if ( hUser32 != NULL )
    {
#ifdef UNICODE
        static \
        MSGBOXWAPI MessageBoxTimeoutW = (MSGBOXWAPI)GetProcAddress(hUser32, "MessageBoxTimeoutW");
        retI = MessageBoxTimeoutW(hWnd, sText, sCaption, uType, 0, dwMilliseconds);
#else
        static \
        MSGBOXWAPI MessageBoxTimeoutW = (MSGBOXWAPI)GetProcAddress(hUser32, "MessageBoxTimeoutA");
        retI = MessageBoxTimeoutA(hWnd, sText, sCaption, uType, 0, dwMilliseconds);
#endif
        FreeLibrary( hUser32 );
    }
    else
    {
        retI = MessageBox(hWnd, sText, sCaption, uType);
    }

    return retI;
}
#endif // _WIN32

const char* _ftoa( float f )
{
    static char fstr[80] = {0};
    snprintf( fstr, 80, "%.2f", f );
    return fstr;
}

////////////////////////////////////////////////////////////////////////////////

WMain::WMain( int argc, char** argv )
 :  _argc( argc ),
    _argv( argv ),
    crossfade_idx( 0 ),
    crossfade_ratio( 0.0f ),
    window( NULL ),
    imgControlBG( NULL ),
    imgSource( NULL ),
    imgHDRed( NULL ),
    busyflag( false ),
    cfgLdr( NULL ),
    timerEventID(-1),
    tmutex(false),
    tstate(-1)
{
    // init FLTK lock mechanism once.
    Fl::lock();

    cfgLdr = new ConfigLoader();
    if ( cfgLdr != NULL )
    {
        cfgLdr->Load();

        if ( cfgLdr->GetLastPath() != NULL )
        {
#ifdef _WIN32
            const char* src = cfgLdr->GetLastPath();
            wchar_t convwt[512] = {0};
            fl_utf8towc( src, strlen( src ),
                         convwt, 512 );
            pathImage = convwt;
#else
            pathImage = cfgLdr->GetLastPath();
#endif /// of _WIN32
        }
    }

    memset( threads, 0, sizeof( pthread_t ) * MAX_THREADS_CNT );

    getEnvironments();
    createComponents();
    registerPopMenu();

#ifdef _WIN32
    extern HINSTANCE fl_display;

    HICON \
    hIconWindowLarge = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         64,
                                         64,
                                         LR_SHARED );
    HICON \
    hIconWindowSmall = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         16,
                                         16,
                                         LR_SHARED );

    if ( window != NULL )
    {
        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_BIG,
                     (LPARAM)hIconWindowLarge );

        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_SMALL,
                     (LPARAM)hIconWindowSmall );
    }
#elif defined(__linux__)
    Fl_RGB_Image* imgIcon = createResImage( "MIOHDR" );
    if( imgIcon != NULL )
    {
        window->icon( imgIcon );

        fl_imgtk::discard_user_rgb_image( imgIcon );
    }
#endif /// of _WIN32
}

WMain::~WMain()
{
    Fl::remove_timeout( WMain::TimerCB, this );
    
    killThread( THREAD_KILL_ALL );

    if ( cfgLdr != NULL )
    {
        cfgLdr->SetLastMode( chsHDR->value() );
        int dt = Fl::screen_num( window->x(), window->y() );
        cfgLdr->SetWindowPos( window->x(),
                              window->y(),
                              window->w(),
                              window->h(),
                              dt );
        cfgLdr->Save();
        delete cfgLdr;
    }
}

int WMain::Run()
{
    if ( window != NULL )
    {
#if defined(USE_FULLSCREEN)
		window->fullscreen();
#endif
        Fl::add_timeout( 0.1f, WMain::TimerCB, this );
        return Fl::run();
    }

    return 0;
}

void* WMain::PThreadCall( ThreadParam* tparam )
{
    if ( tparam == NULL )
        return NULL;

    long pLL = tparam->paramL;
    bool bnflush = false;

    switch ( pLL )
    {
        case THREAD_JOB_APPLYING_HDR:
            {
                window->cursor( FL_CURSOR_WAIT );
                strProgress = "Now on image processing,\n"
                              "Please wait a minute ...";
                boxProgress->label( strProgress.c_str() );
                boxProgress->show();

                int curhdr = chsHDR->value();

                grpControl->deactivate();

                fl_imgtk::discard_user_rgb_image( imgHDRed );

                float paramf[4] = {0.0f, };

                for( unsigned cnt=0; cnt<4; cnt++ )
                {
                    const char* convs2f = inpParam[ cnt ]->value();

                    if ( convs2f != NULL )
                    {
                        paramf[ cnt ] = atof( convs2f );
                    }
                }

                // store current config values ...
                if ( cfgLdr != NULL )
                {
                    cfgLdr->SetLastParams( curhdr, paramf );
                }

                switch( curhdr )
                {
                    case 0: /// log mapping
                        {
                            imgHDRed = \
                                fl_imgtk::tonemapping_drago( imgSource,
                                                             paramf[0],
                                                             paramf[1] );
                        }
                        break;

                    case 1: /// Reinhard
                        {
                            imgHDRed = \
                                fl_imgtk::tonemapping_reinhard( imgSource,
                                                                paramf[0],
                                                                paramf[1],
                                                                paramf[2],
                                                                paramf[3] );
                        }
                        break;

                    case 2: /// enhanced CLAHE
                        {
                            imgHDRed = \
                                fl_imgtk::CLAHE( imgSource,
                                                 (unsigned)paramf[0],
                                                 (unsigned)paramf[1],
                                                 paramf[2] );
                        }
                        break;
                }

                bool bContinue2Histo = false;

                Fl::lock();

                if ( imgHDRed != NULL )
                {
                    boxImgPreView->image( imgHDRed, sensFitting( imgHDRed ) );
                    bContinue2Histo = true;
                    btnCopy2Clip->show();
                    btnSaveTo->show();
                }
                else
                {
                    btnCopy2Clip->hide();
                    btnSaveTo->hide();
                }

                grpControl->activate();
                btnRotLeft->activate();
                btnRotRight->activate();
                btnCopy2Clip->activate();
                btnApply->activate();
                grpControl->damage( 0 );

                window->cursor( FL_CURSOR_DEFAULT );
                boxProgress->hide();

                window->redraw();
                Fl::unlock();

                if ( bContinue2Histo == false )
                {
                    bnflush = true;
                    Fl::awake();
                    break;
                }
            }
            // do not break here,
            // just goes down for update histogram.

        case THREAD_JOB_UPDATE_HISTO:
            {
                // check for cached image exists.
                if ( boxImgPreView->resizedimage() != NULL )
                {
                    Fl::lock();
                    boxImgPreView->updateHistogram();
                    boxImgPreView->redraw();
                    Fl::unlock();

                    bnflush = true;
                    Fl::awake();
                }
            }
            break;

        case THREAD_JOB_SAVE_FILE:
            {
                fl_pngwriter_setcallback_inst( this );
                fl_pngwriter_setcallback( WMain::PNGWriteCB );
                Fl::lock();
                strProgress = "preparing write image ...";
                boxProgress->label( strProgress.c_str() );
                boxProgress->redraw();
                Fl::unlock();
                Fl::awake();

                fl_image_write_to_pngfile( imgHDRed, savefn.c_str(), 7 );

                fl_pngwriter_unsetcallback();

                // Let disappear overlay ..
                Fl::lock();
                boxProgress->hide();
                window->activate();
                window->redraw();
                window->cursor( FL_CURSOR_DEFAULT );
                Fl::unlock();
                Fl::awake();

                btnCopy2Clip->activate();
                btnSaveTo->activate();
            }
            break;
    }

    boxImgPreView->take_focus();

    // Make window drawing called back from system.
    window->redraw();

#ifndef __APPLE__
	if ( bnflush == true )
	{
#ifdef _WIN32
    	// Windows using invalidateRect.
    	InvalidateRect( fl_xid(window), NULL, TRUE );
#else
        Fl::awake();
#endif /// of _WIN32
	}
#endif // of __APPLE__

    delete threads[ pLL ];
    threads[ pLL ] = NULL;

    pthread_exit( NULL );

    return NULL;
}

void WMain::WidgetCall( Fl_Widget* w )
{
    if ( w == window )
    {
        return;
    }

    if ( w == chsHDR )
    {
        int curidx = chsHDR->value();

        switch( curidx )
        {
            case 0: /// log mapping.
                {
                    inpParam[0]->tooltip( "Gamma ( -10.0 ~ 10.0 )\n"
                                          "  1.0 = no correction\n"
                                          "  2.2 = original paper" );
                    inpParam[1]->tooltip( "Exposure ( 0.0 ~ )" );
                    inpParam[2]->tooltip( "N/A" );
                    inpParam[3]->tooltip( "N/A" );

                    inpParam[0]->activate();
                    inpParam[1]->activate();
                    inpParam[2]->deactivate();
                    inpParam[3]->deactivate();

                    if ( cfgLdr != NULL )
                    {
                        float vfs[2] = {0.f};
                        cfgLdr->GetLastParams( curidx, vfs );
                        inpParam[0]->value( _ftoa( vfs[0] ) );
                        inpParam[1]->value( _ftoa( vfs[1] ) );
                        inpParam[2]->value( "" );
                        inpParam[3]->value( "" );
                    }
                    else
                    {
                        resetParameters();
                    }
                }
                break;

            case 1: /// Reinhard.
                {
                    inpParam[0]->tooltip( "Intensity ( -8.0 ~ 8.0 )\n"
                                          "Recommend to do not be changed" );
                    inpParam[1]->tooltip( "Contrast ( 0.3 ~ 1.0 )" );
                    inpParam[2]->tooltip( "Adaptation ( 0.0 ~ 1.0 )" );
                    inpParam[3]->tooltip( "Color Correction (0.0 ~)" );

                    inpParam[0]->activate();
                    inpParam[1]->activate();
                    inpParam[2]->activate();
                    inpParam[3]->activate();

                    if ( cfgLdr != NULL )
                    {
                        float vfs[4] = {0.f};
                        cfgLdr->GetLastParams( curidx, vfs );
                        inpParam[0]->value( _ftoa( vfs[0] ) );
                        inpParam[1]->value( _ftoa( vfs[1] ) );
                        inpParam[2]->value( _ftoa( vfs[2] ) );
                        inpParam[3]->value( _ftoa( vfs[3] ) );
                    }
                    else
                    {
                        resetParameters();
                    }
                }
                break;

            case 2: /// eCLAHE
                {
                    inpParam[0]->tooltip( "Divider width ( 2 ~ 16 )" );
                    inpParam[1]->tooltip( "Divider height ( 2 ~ 16 )" );
                    inpParam[2]->tooltip( "Clipping limit ( 1.0 ~ 100.0 )" );
                    inpParam[3]->tooltip( "N/A" );

                    inpParam[0]->activate();
                    inpParam[1]->activate();
                    inpParam[2]->activate();
                    inpParam[3]->deactivate();

                    if ( cfgLdr != NULL )
                    {
                        float vfs[3] = {0.f};
                        cfgLdr->GetLastParams( curidx, vfs );
                        inpParam[0]->value( _ftoa( vfs[0] ) );
                        inpParam[1]->value( _ftoa( vfs[1] ) );
                        inpParam[2]->value( _ftoa( vfs[2] ) );
                        inpParam[3]->value( "" );
                    }
                    else
                    {
                        resetParameters();
                    }
                }
                break;
        }

        grpControl->redraw();

        return;
    }

    if ( w == btnReset )
    {
        resetParameters();
    }

    if ( w == btnLoad )
    {
        loadImage();

        return;
    }

    if ( w == btnApply )
    {
        btnApply->deactivate();
        btnRotLeft->deactivate();
        btnRotRight->deactivate();

        killThread( THREAD_KILL_ALL );
#ifndef _WIN32
        // Some Xorg system need this:
        usleep( 100 );
#endif 
        createThread( THREAD_JOB_APPLYING_HDR );

        return;
    }

    if ( w == btnCopy2Clip )
    {
        if ( imgHDRed != NULL )
        {
            // prevent double call ...
            if ( tstate >= 100 )
                return;

            tstate = 102;
            btnCopy2Clip->deactivate();
            btnSaveTo->deactivate();
            btnRotLeft->deactivate();
            btnRotRight->deactivate();
            
            strProgress = "Copying to clipboard ...";
            boxProgress->show();
            Fl::awake();
            timerEventID = 109;
            if ( Fl::has_timeout( WMain::TimerCB, this ) > 0 )
            {
                Fl::repeat_timeout( 0.08f, WMain::TimerCB, this );
            }
            else
            {
                Fl::add_timeout( 0.08f, WMain::TimerCB, this );
            }
 
            Fl::wait();

            if ( copy2clipboard(1) == true )
            {
                strProgress = "Image copied to clipboard";
            }
            else
            {
                strProgress = "Failed to copy image to clipboard";
            }
            btnRotLeft->activate();
            btnRotRight->activate();
            btnCopy2Clip->activate();
            btnSaveTo->activate();

            timerEventID = 110;
            if ( Fl::has_timeout( WMain::TimerCB, this ) > 0 )
            {
                Fl::repeat_timeout( 0.08f, WMain::TimerCB, this );
            }
            else
            {
                Fl::add_timeout( 0.08f, WMain::TimerCB, this );
            }
       }

        return;
    }

    if ( w == btnSaveTo )
    {
        if ( imgHDRed != NULL )
        {
            btnCopy2Clip->deactivate();
            btnSaveTo->deactivate();
            window->redraw();

            strProgress = "... writing in progress ...";
            boxProgress->label( strProgress.c_str() );

            window->cursor( FL_CURSOR_WAIT );
            boxProgress->show();
            window->redraw();
            Fl::awake();

            if ( saveImageProc() == false )
            {
                // Let disappear overlay ..
                Fl::lock();
                boxProgress->hide();
                window->activate();
                window->redraw();
                window->cursor( FL_CURSOR_DEFAULT );
                Fl::unlock();
                Fl::awake();

                btnCopy2Clip->activate();
                btnSaveTo->activate();
            }
        }

        return;
    }

    if ( w == btnRotLeft )
    {
        grpControl->deactivate();
        rotateLeft();
        grpControl->activate();
        grpControl->redraw();

        return;
    }

    if ( w == btnRotRight )
    {
        grpControl->deactivate();
        rotateRight();
        grpControl->activate();
        grpControl->redraw();
        return;
    }

    if ( w == popMenu )
    {
        int pmsel = popMenu->value();
        switch( pmsel )
        {
            case 0: /// Copy to clipboard
                btnCopy2Clip->do_callback();
                break;

            case 1: /// Rotate Left
                grpControl->deactivate();
                rotateLeft();
                grpControl->activate();
                grpControl->redraw();
                break;

            case 2: /// Rotate Right
                grpControl->deactivate();
                rotateRight();
                grpControl->activate();
                grpControl->redraw();
                break;

            case 3: /// Hide top menu
                if ( grpControl->visible_r() > 0 )
                {
                    grpControl->hide();
                }
                else
                {
                    grpControl->show();
                }
                break;
        }

        return;
    }
}

void WMain::OnMouseMove( void* w, int x, int y, bool inside )
{

}

void WMain::OnMouseClick( void* w, int x, int y, unsigned btn )
{
    if ( btn == FL_BUTTON3 )
    {
        if ( popMenu != NULL )
        {
            popMenu->position( x, y );
            popMenu->show();
        }
    }
}

void WMain::OnKeyPressed( void* w, unsigned short k, int s, int c, int a )
{
#ifdef DEBUG
    printf("OnKeyPressed!\n");
#endif // DEBUG
}

void WMain::OnDropFiles( void* w, const char* files )
{
    if ( busyflag == true )
        return;

    killThread( THREAD_KILL_ALL );

#ifdef DEBUG
    printf("OnDropFiles : %s\n", files);
#endif // DEBUG

    if ( ( w == boxImgPreView ) && ( files != NULL ) )
    {
        // decode UTF-8 strings to wide characters.

        size_t decodelen = strlen( files );
        if ( decodelen > 0 )
        {
#ifdef SUPPORT_WCHAR
            wchar_t* decodedstr = new wchar_t[ decodelen + 1 ];
            if ( decodedstr != NULL )
            {
                fl_utf8towc( files, strlen( files ),
                             decodedstr, decodelen + 1 );

                wstring tmpfiles = decodedstr;

                delete[] decodedstr;

                vector<wstring> tmpfilelist;

                tmpfilelist = split( tmpfiles, L'\n' );
#else
            const char* decodedstr = files;
            if ( decodedstr != NULL )
            {
                vector<string> tmpfilelist;
                string tmpfiles = decodedstr;

                tmpfilelist = split( tmpfiles, '\n' );
#endif /// of SUPPORT_WCHAR
                if ( tmpfilelist.size() > 0 )
                {
#ifdef __linux__
                    for( size_t cnt=0; cnt<tmpfilelist.size(); cnt++ )
                    {
#ifdef DEBUG
                        printf( "splitted file path : %s -> ",
                                tmpfilelist[cnt].c_str() );
#endif /// of DEBUG

                        decodeURLpath( tmpfilelist[cnt] );
#ifdef DEBUG
                        printf( "%s\n",
                                tmpfilelist[cnt].c_str() );
#endif /// of DEBUG
                   }
#endif /// of  __linux__

                    for( size_t cnt=0; cnt<tmpfilelist.size(); cnt++ )
                    {
                        // check while image loads.
                        char* tmpbuff = NULL;
                        size_t tmpbuffsz = 0;
#ifdef DEBUG
                        printf( "Loading image %s ...\n",
                                tmpfilelist[cnt].c_str() );
#endif /// of DEBUG
                        int ret = testImageFile( tmpfilelist[ cnt ].c_str(), &tmpbuff, &tmpbuffsz );
                        if ( ( ret > 0 ) && ( tmpbuffsz > 0 ) )
                        {
#ifdef DEBUG
                            printf( "-->loaded.\n" );
#endif /// of DEBUG
                            Fl_RGB_Image* tmpimg = NULL;

                            switch( ret )
                            {
                                case 1: /// JPEG
                                    tmpimg = new Fl_JPEG_Image( "JPGIMG", (const uchar*)tmpbuff );
                                    break;

                                case 2: /// PNG
                                    tmpimg = new Fl_PNG_Image( "PNGIMAGE", (const uchar*)tmpbuff, tmpbuffsz );
                                    break;

                                case 3: /// BMP
                                    tmpimg = fl_imgtk::createBMPmemory( (const char*)tmpbuff, tmpbuffsz );
                                    break;
                            }

                            delete[] tmpbuff;

                            if ( tmpimg != NULL )
                            {
                                fl_imgtk::discard_user_rgb_image( imgSource );

                                imgSource = tmpimg;

                                boxImgPreView->image( imgSource, sensFitting( imgSource ) );
                                boxImgPreView->redraw();

                                pathImage = stripFilePath( tmpfilelist[ cnt ].c_str() );
                                imgFName  = stripFileName( tmpfilelist[ cnt ].c_str() );

                                // Store current path ...
#ifdef SUPPORT_WCHAR
                                if ( cfgLdr != NULL )
                                {
                                    //convert to UTF8.
                                    char convu8[512] = {0};
                                    fl_utf8fromwc( convu8,
                                                   512,
                                                   pathImage.c_str(),
                                                   pathImage.size() );
                                    cfgLdr->SetLastPath( convu8 );
                                }
#else
                                if ( cfgLdr != NULL )
                                {
                                    cfgLdr->SetLastPath( pathImage.c_str() );
                                }

#endif /// of SUPPORT_WCHAR
                                // Convert source image to new name to be saved.
#ifdef SUPPORT_WCHAR
                                wstring tmpwstr = imgFName;
                                size_t fpos = tmpwstr.find_last_of( L'.' );
                                if ( fpos != string::npos )
                                {
                                    tmpwstr = tmpwstr.substr( 0, fpos );
                                }
                                char tmputf8[1024] = {0,};
                                fl_utf8fromwc( tmputf8, 1024, tmpwstr.c_str(), tmpwstr.size() );
                                imgFNameUTF8 = tmputf8;
#else
                                string tmpstr = imgFName;
                                size_t fpos = tmpstr.find_last_of( '.' );
                                if ( fpos != string::npos )
                                {
                                    tmpstr = tmpstr.substr( 0, fpos );
                                }
                                imgFNameUTF8 = tmpstr;
#endif
                                // Update window title.
                                setdefaultwintitle();
                                string tmpwintitle = wintitlestr;
                                snprintf( wintitlestr, MAX_WINTITLE_LEN,
                                          "%s : %s", imgFNameUTF8.c_str(), tmpwintitle.c_str() );
                                window->label( wintitlestr );

                                updateImageHistogram();

                                fl_imgtk::discard_user_rgb_image( imgHDRed );

                                btnApply->show();
                                btnRotLeft->show();
                                btnRotRight->show();

                                btnCopy2Clip->hide();
                                btnSaveTo->hide();

                                grpControl->redraw();
                                return;
                            }
                        }

                    }
                }
            }
        }

    }
}

void WMain::OnDrawCompleted()
{
#if !defined(DISABLE_BLURBACK)
    Fl::lock();
    boxControl->hide();
    boxControl->image( NULL );

    if ( imgControlBG != NULL )
    {
        delete imgControlBG;
        imgControlBG = NULL;
    }

    size_t wdg_x = boxControl->x();
    size_t wdg_y = boxControl->y();
    size_t wdg_w = boxControl->w();
    size_t wdg_h = boxControl->h();
    bool   ndnscl = false;
 
 #ifdef _WIN32
    // Get Desktop physical height and logical height.
    HDC hdc = GetDC( NULL );
    float logicalY = GetDeviceCaps( hdc, VERTRES );
    float physicalY = GetDeviceCaps( hdc, DESKTOPVERTRES );
    ReleaseDC( NULL, hdc );

    // Let get DPI ratio for scaled --
    float dpiR = physicalY / logicalY;
    // 100% = 1.0f
    // 125% = 1.25f ...
    if ( dpiR > 1.0f )
    {
        // High DPI with asmv3-GDIscaling element in 
        // manifest XML makes actual drawing DC to
        // double size, not actual DPI ratio when over 100%.
        // It is a reason for looks clear-graphic scaling by Microsoft.
        wdg_x *= 2;
        wdg_y *= 2;
        wdg_w *= 2;
        wdg_h *= 2;
        ndnscl = true;
    }
#endif /// of _WIN32
    uchar* widgetbuff = new uchar[ wdg_w * wdg_h * 3 + 1 ];
    if ( widgetbuff != NULL )
    {
        // Read current window pixels to buffer for create a new Fl_RGB_Image.
        fl_read_image( widgetbuff, wdg_x, wdg_y, wdg_w, wdg_h );

        Fl_RGB_Image* imgcntl = new Fl_RGB_Image( widgetbuff, wdg_w, wdg_h, 3 );

        if ( imgcntl != NULL )
        {
            imgcntl->alloc_array = 1;
#ifdef _WIN32
            if ( ndnscl == true )
            {
                Fl_RGB_Image* imgdnscl = fl_imgtk::rescale( imgcntl,
                                                            boxControl->w(),
                                                            boxControl->h() );
                if ( imgdnscl != NULL )
                {
                    // let's replace image to downscaled.
                    delete imgcntl;
                    imgcntl = imgdnscl;
                }
            }
#endif /// of _WIN32
            fl_imgtk::brightness_ex( imgcntl, -50 );
            fl_imgtk::draw_line( imgcntl,
                                 0, 0,
                                 boxControl->w(), 0,
                                 0xFFFFFFFF );
            fl_imgtk::draw_line( imgcntl,
                                 0, 2,
                                 boxControl->w(), 2,
                                 0xFFFFFFFF );
            fl_imgtk::draw_line( imgcntl,
                                 0, boxControl->h()-1,
                                 boxControl->w(), boxControl->h()-1,
                                 0xFFFFFFFF );
            fl_imgtk::blurredimage_ex( imgcntl, 8 );
            fl_imgtk::draw_line( imgcntl,
                                 0, boxControl->h()-1,
                                 boxControl->w(), boxControl->h()-1,
                                 0xFFFFFF1F );

            imgControlBG = imgcntl;
            boxControl->image( imgControlBG );
        }
        else
        {
            delete[] widgetbuff;
        }
    }
    Fl::unlock();
    boxControl->show();
    grpControl->redraw();
#endif /// of !defined(DISABLE_BLURBACK)
}

void WMain::getEnvironments()
{
#ifdef _WIN32
    pathHome        = _wgetenv( L"USERPROFILE" );
    pathSystemBase  = _wgetenv( L"SYSTEMROOT" );
    pathUserData    = _wgetenv( L"LOCALAPPDATA" );
    pathUserRoaming = _wgetenv( L"APPDATA" );
#ifdef DEBUG
    wprintf( L"pathHome        : %S\n", pathHome.c_str() );
    wprintf( L"pathSystemBase  : %S\n", pathSystemBase.c_str() );
    wprintf( L"pathUserData    : %S\n", pathUserData.c_str() );
    wprintf( L"pathUserRoaming : %S\n", pathUserRoaming.c_str() );
#endif // DEBUG
#elif defined(__APPLE__)
    resourcebase = _argv[0];
    size_t fpos = resourcebase.find_last_of('/');
    if ( fpos != string::npos )
    {
        resourcebase = resourcebase.substr( 0, fpos );

        fpos = resourcebase.find_last_of('/');

        if ( fpos != string::npos )
        {
            resourcebase = resourcebase.substr( 0, fpos );
            resourcebase += "/Resources";
        }
    }

    pathHome        = getenv( "HOME" );
    pathUserData    = pathHome;
    pathUserRoaming = pathHome;
#else /// must be Linux
    resourcebase = "/usr/local/share/images/MIOHDR";

    if ( access( resourcebase.c_str(), 0 ) != 0 )
    {
        resourcebase = _argv[0];
        size_t fpos = resourcebase.find_last_of('/');
        if ( fpos != string::npos )
        {
            resourcebase = resourcebase.substr( 0, fpos );
            resourcebase += "/res";
        }
    }

    pathHome        = getenv( "HOME" );
    pathUserData    = pathHome;
    pathUserRoaming = pathHome;

#ifdef DEBUG
	printf("#DEBUG.PATHES\n");
	printf("HOME: %s\n", pathHome.c_str());
	printf("resourcebase: %s\n",resourcebase.c_str());
#endif /// of DEBUG
#endif /// of _WIN32, __APPLE__ and __linux__
}

void WMain::setdefaultwintitle()
{
    memset( wintitlestr, 0, MAX_WINTITLE_LEN );
    snprintf( wintitlestr, MAX_WINTITLE_LEN,
              "%s v.%s", DEF_APP_NAME, APP_VERSION_STR );
}

void WMain::resetParameters()
{
    int curidx = chsHDR->value();
    float vfs[4] = {0.0f};
    bool modded = false;

    switch( curidx )
    {
        case 0:
            vfs[0] = 1.0f;

            inpParam[0]->value( _ftoa( vfs[0] ) );
            inpParam[1]->value( _ftoa( vfs[1] ) );
            inpParam[2]->value( "" );
            inpParam[3]->value( "" );

            modded = true;
            break;

        case 1:
            vfs[0] = 1.0f;
            vfs[1] = 0.3f;
            vfs[2] = 0.5f;

            inpParam[0]->value( _ftoa( vfs[0] ) );
            inpParam[1]->value( _ftoa( vfs[1] ) );
            inpParam[2]->value( _ftoa( vfs[2] ) );
            inpParam[3]->value( _ftoa( vfs[3] ) );

            modded = true;
            break;

        case 2:
            vfs[0] = 2.0f;
            vfs[1] = 2.0f;
            vfs[2] = 1.9f;
  
            inpParam[0]->value( _ftoa( vfs[0] ) );
            inpParam[1]->value( _ftoa( vfs[1] ) );
            inpParam[2]->value( _ftoa( vfs[2] ) );
            inpParam[3]->value( "" );
           
            modded = true;
            break;
    }

    if ( ( cfgLdr != NULL ) && ( modded == true ) )
    {
        cfgLdr->SetLastParams( curidx, vfs );
    }
}

void WMain::createComponents()
{
    setdefaultwintitle();
    
    unsigned ww = 790;
    unsigned wh = 440;

    window = new Fl_Double_Window( ww, wh, wintitlestr );
    if ( window != NULL )
    {
        window->color( DEF_WIN_COLOR_BG );
        window->labelfont( DEF_WIDGET_FNT );
        window->labelsize( DEF_WIDGET_FSZ );
        window->labelcolor( DEF_WIN_COLOR_FG );

        // continue to child components ...
        window->begin();

            boxImgPreView = new Fl_ImageViewer( 0, 0, window->w(), window->h() );
            if ( boxImgPreView != NULL )
            {
                boxImgPreView->notifier( this );
                boxImgPreView->box( FL_NO_BOX );
                boxImgPreView->resizemethod( fl_imgtk::BILINEAR, true );
            }

            grpControl = new Fl_Group( 0, 0, window->w(), 50 );

            if ( grpControl != NULL )
            {
                grpControl->begin();

#if defined(DISABLE_BLURBACK)
                boxControl = new Fl_TransBox( grpControl->x(), grpControl->y(),
                                              grpControl->w(), grpControl->h() );
                if ( boxControl != NULL )
                {
                    boxControl->color( 0x15151500 );
                }
#else
                boxControl = new Fl_Box( grpControl->x(), grpControl->y(),
                                         grpControl->w(), grpControl->h() );
                if ( boxControl != NULL )
                {
                    boxControl->box( FL_NO_BOX );
                }
#endif /// of defined(DISABLE_BLURBACK)
                int gc_a_w = 530;

                grpControlActive = new Fl_Group( grpControl->x(), grpControl->y(),
                                                 gc_a_w, grpControl->h() );

                if ( grpControlActive != NULL )
                {
                    grpControlActive->box( FL_NO_BOX );
                    grpControlActive->begin();
                }

                int l_x = 50;
                int l_y = 12;
                int l_w = 120;
                int l_h = 25;

                chsHDR = new Fl_Choice( l_x, l_y, l_w, l_h );
                if ( chsHDR != NULL )
                {
                    chsHDR->box( FL_NO_BOX );
                    chsHDR->color( window->color() );
                    chsHDR->labelcolor( window->labelcolor() );
                    chsHDR->textcolor( 0xFF663300 );
                    chsHDR->textsize( window->labelsize() + 2 );
                    chsHDR->labelfont( window->labelfont() );
                    chsHDR->labelsize( window->labelsize() );
                    chsHDR->when( FL_WHEN_CHANGED );
                    chsHDR->clear_visible_focus();

                    Fl_SVG_Image* psvg = createResSVGImage( "img_eyefilter" );
                    if ( psvg != NULL )
                    {
                        chsHDR->image( psvg );
                        psvg->scale( chsHDR->h(), chsHDR->h() );
                    }
                    chsHDR->tooltip( "HDR types" );

                    chsHDR->add( "log mapping" );
                    chsHDR->add( "Reinhard '05" );
                    chsHDR->add( "eCLAHE" );

                    chsHDR->value( 0 );
                    if ( cfgLdr != NULL )
                    {
                        chsHDR->value( cfgLdr->GetLastMode() );
                    }
                    chsHDR->callback( WMain::WidgetCB, this );
                }

                l_y = 12;
                l_h = 25;
                l_x += l_w + 40;
                l_w = 50;

                for( unsigned cnt=0; cnt<4; cnt++ )
                {
                    char tmpstr[32] = {0,};
                    snprintf( tmpstr, 32, "P.%d:", cnt+1);
                    inpParam[ cnt ] = new Fl_Input( l_x, l_y, 
                                                    l_w, l_h, 
                                                    strdup( tmpstr ) );
                    if ( inpParam[ cnt ] != NULL )
                    {
                        inpParam[ cnt ]->box( FL_THIN_DOWN_BOX );
                        inpParam[ cnt ]->type( FL_FLOAT_INPUT );
                        inpParam[ cnt ]->color( window->color() );
                        inpParam[ cnt ]->labelcolor( window->labelcolor() );
                        inpParam[ cnt ]->labelfont( window->labelfont() );
                        inpParam[ cnt ]->labelsize( window->labelsize() + 2 );
                        inpParam[ cnt ]->textcolor( 0xFF663300 );
                        inpParam[ cnt ]->textfont( FL_COURIER );
                        inpParam[ cnt ]->textsize( window->labelsize() + 2 );
                        inpParam[ cnt ]->cursor_color( 0xFF996600 );
                        inpParam[ cnt ]->selection_color( 0xAAAAAA00 );
                        inpParam[ cnt ]->value( "0.0" );
                    }

                    if ( cnt + 1 < 4 )
                    {
                        l_x += l_w + 30;
                    }
                    else
                    {
                        l_x += l_w + 5;
                    }
                }

                btnReset = new Fl_Button( l_x, l_y, 20, l_h, "R" );
                if ( btnReset != NULL )
                {
                    btnReset->box( FL_THIN_UP_BOX );
                    btnReset->color( window->color() );
                    btnReset->labelcolor( window->labelcolor() );
                    btnReset->labelfont( window->labelfont() );
                    btnReset->labelsize( window->labelsize() );
                    btnReset->clear_visible_focus();
                    btnReset->callback( WMain::WidgetCB, this );
                }

                if ( grpControlActive != NULL )
                {
                    grpControlActive->end();
                }


                grpControlEmpty = new Fl_Group( gc_a_w, grpControl->y(), 10, grpControl->h() );
                if( grpControlEmpty != NULL )
                {
                    grpControlEmpty->begin();

                    Fl_Box* boxSep = new Fl_Box( grpControlEmpty->x(),
                                                 grpControlEmpty->y(),
                                                 grpControlEmpty->w(),
                                                 grpControlEmpty->h(),
                                                 "@2line" );
                    if ( boxSep != NULL )
                    {
                        boxSep->box( FL_NO_BOX );
                        boxSep->labelcolor( 0x77777700 );
                    }
                    grpControlEmpty->end();
                }

                l_x = gc_a_w + 10;

                grpControlTool = new Fl_Group( l_x, grpControl->y(),
                                               grpControl->w() - l_x, grpControl->h() );
                if ( grpControlTool != NULL )
                {
                    grpControlTool->begin();
                }

                l_y = 10;
                l_h = 30;
                l_w = 30;

                btnLoad = new Fl_Image_Button( l_x, l_y, l_w, l_h );
                if ( btnLoad != NULL )
                {
                    btnLoad->image( createResSVGImage( "img_load" ) );
                    btnLoad->tooltip( "Load image" );
                    btnLoad->callback( WMain::WidgetCB, this );
                }

                l_x += l_w + 5;

                btnRotLeft = new Fl_Image_Button( l_x, l_y, l_w, l_h );
                if ( btnRotLeft != NULL )
                {
                    btnRotLeft->image( createResSVGImage( "img_rot_l45d" ) );
                    btnRotLeft->tooltip( "Rotate image to 90 degree in clockwise" );
                    btnRotLeft->callback( WMain::WidgetCB, this );
                    btnRotLeft->hide();
                }

                l_x += l_w + 5;

                btnRotRight = new Fl_Image_Button( l_x, l_y, l_w, l_h );
                if ( btnRotRight != NULL )
                {
                    btnRotRight->image( createResSVGImage( "img_rot_r45d" ) );
                    btnRotRight->tooltip( "Rotate image to 90 degree in counterclockwise" );
                    btnRotRight->callback( WMain::WidgetCB, this );
                    btnRotRight->hide();
                }

                l_x += l_w + 5;

                btnApply = new Fl_Image_Button( l_x, l_y, l_w, l_h );
                if ( btnApply != NULL )
                {
                    btnApply->image( createResSVGImage( "img_apply" ) );
                    btnApply->tooltip( "Apply changes" );
                    btnApply->callback( WMain::WidgetCB, this );
                    btnApply->hide();
                }

                l_x += l_w + 5;

                btnCopy2Clip = new Fl_Image_Button( l_x, l_y, l_w, l_h );
                if ( btnCopy2Clip != NULL )
                {
                    btnCopy2Clip->image( createResSVGImage( "img_copy2clipboard" ) );
                    btnCopy2Clip->tooltip( "Copy HDR image to clipboard" );
                    btnCopy2Clip->callback( WMain::WidgetCB, this );
                    btnCopy2Clip->hide();
                }

                l_x += l_w + 5;

                btnSaveTo = new Fl_Image_Button( l_x, l_y, l_w, l_h );
                if ( btnSaveTo != NULL )
                {
                    btnSaveTo->box( FL_NO_BOX );
                    btnSaveTo->color( 0xFFFFFF00 );
                    btnSaveTo->labelcolor( window->labelcolor() );
                    btnSaveTo->labelfont( window->labelfont() );
                    btnSaveTo->labelsize( window->labelsize() );
                    btnSaveTo->clear_visible_focus();

                    btnSaveTo->image( createResSVGImage( "img_save" ) );
                    btnSaveTo->tooltip( "Save image to a PNG ..." );

                    btnSaveTo->callback( WMain::WidgetCB, this );

                    btnSaveTo->hide();
                }

                if ( grpControlTool != NULL )
                {
                    grpControlTool->end();
                }

                grpControl->end();
                grpControl->resizable( grpControlEmpty );
            }


            grpImageView = new Fl_Group( 0, 50, window->w(), window->h() - 50 );

            if ( grpImageView != NULL )
            {
                grpImageView->begin();

                // Something here ...

                grpImageView->end();
                grpImageView->hide();
            }

            // -- ungrouped control for prev/next image button
            //    and progress box.
            
            // currently no implemented.
            btnPrevImg = NULL; 
            btnNextImg = NULL;

            boxProgress = new Fl_TransBox( 0, 0, window->w(), window->h() );
            if ( boxProgress != NULL )
            {
                boxProgress->labelfont( window->labelfont() );
                boxProgress->labelsize( window->labelsize() * 3 );
                boxProgress->labelcolor( window->labelcolor() );
                boxProgress->color( 0x15151500 );
                boxProgress->hide();
            }

        window->end();

        if ( grpImageView != NULL )
        {
            window->resizable( grpImageView );
            window->size_range( window->w(), window->h() );
        }

        if ( chsHDR != NULL )
        {
            chsHDR->do_callback();
        }

        if ( cfgLdr != NULL )
        {
            int wx = 0;
            int wy = 0;
            int ww = 0;
            int wh = 0;
            int dt = 0;

            cfgLdr->GetWindowPos( wx, wy, ww, wh, dt );

            int maxdt = Fl::screen_count();

            if ( dt > maxdt )
            {
                // seems screen changed.
                dt = maxdt;
                // reset to position 20,20.
                wx = 20;
                wy = 20;
            }

            if ( ww < window->w() )
                ww = window->w();

            if ( wh < window->h() )
                wh = window->h();
            
            window->resize( wx, wy, ww, wh );
        }
        window->show();
    }
}

void WMain::registerPopMenu()
{
    popMenu = new Fl_Menu_Button( 0, 0, window->w(), window->h() );
    if ( popMenu != NULL )
    {
        popMenu->type( Fl_Menu_Button::POPUP3 );
        popMenu->color( window->color(), window->labelcolor() );
        popMenu->selection_color( window->color() );
        popMenu->labelfont( window->labelfont() );
        popMenu->textfont( window->labelfont() );
        popMenu->labelsize( window->labelsize() );
        popMenu->textsize( window->labelsize() );
        popMenu->labelcolor( window->labelcolor() );
        popMenu->textcolor( window->labelcolor() );

#ifdef __APPLE__
        int ckey[] = {
            FL_COMMAND + 'c',
            FL_COMMAND + FL_Left,
            FL_COMMAND + FL_Right,
            0x20 };
#else
        int ckey[] = {
            FL_CTRL + 'c',
            FL_CTRL + FL_Left,
            FL_CTRL + FL_Right,
            0x20 };
#endif /// of __APPLE__

        popMenu->add( "_Copy to clipboard\t", ckey[0], WMain::WidgetCB, this, 0 );
        popMenu->add( "Rotate Left       \t", ckey[1], WMain::WidgetCB, this, 0 );
        popMenu->add( "_Rotate Right     \t", ckey[2], WMain::WidgetCB, this, 0 );
        popMenu->add( "Show&&Hide top menu\t", ckey[3], WMain::WidgetCB, this, 0 );

        // Ok. then, insert popup to main window.
        window->add( popMenu );
    }
}

void WMain::updateImageHistogram()
{
    createThread( THREAD_JOB_UPDATE_HISTO );
}

void WMain::updateCrossFadeBG()
{
    // Check current cross-fading condition ...

}

#ifdef SUPPORT_WCHAR
int WMain::testImageFile( const wchar_t* imgfp, char** buff,size_t* buffsz )
{
    int reti = -1;

    if ( imgfp != NULL )
    {
        FILE* fp = _wfopen( imgfp, L"rb" );
        if ( fp != NULL )
        {
            fseek( fp, 0L, SEEK_END );
            size_t flen = ftell( fp );
            fseek( fp, 0L, SEEK_SET );

            if ( flen > 32 )
            {
                // Test
                char testbuff[32] = {0,};

                fread( testbuff, 1, 32, fp );
                fseek( fp, 0, SEEK_SET );

                const uchar jpghdr[3] = { 0xFF, 0xD8, 0xFF };

                // is JPEG ???
                if( strncmp( &testbuff[0], (const char*)jpghdr, 3 ) == 0 )
                {
                    reti = 1; /// JPEG.
                }
                else
                if( strncmp( &testbuff[1], "PNG", 3 ) == 0 )
                {
                    reti = 2; /// PNG.
                }
                else
                if( strncmp( &testbuff[0], "BM", 2 ) == 0 )
                {
                    reti = 3; /// BMP.
                }

                if ( reti > 0 )
                {
                    *buff = new char[ flen ];
                    if ( *buff != NULL )
                    {
                        fread( *buff, 1, flen, fp );

                        if( buffsz != NULL )
                        {
                            *buffsz = flen;
                        }
                    }
                }
            }

            fclose( fp );
        }
    }

    return reti;
}
#else
int WMain::testImageFile( const char* imgfp, char** buff,size_t* buffsz )
{
    int reti = -1;

    if ( imgfp != NULL )
    {
        FILE* fp = fopen( imgfp, "rb" );
        if ( fp != NULL )
        {
            fseek( fp, 0L, SEEK_END );
            size_t flen = ftell( fp );
            fseek( fp, 0L, SEEK_SET );

            if ( flen > 32 )
            {
                // Test
                char testbuff[32] = {0,};

                fread( testbuff, 1, 32, fp );
                fseek( fp, 0, SEEK_SET );

                const uchar jpghdr[3] = { 0xFF, 0xD8, 0xFF };

                // is JPEG ???
                if( strncmp( &testbuff[0], (const char*)jpghdr, 3 ) == 0 )
                {
                    reti = 1; /// JPEG.
                }
                else
                if( strncmp( &testbuff[1], "PNG", 3 ) == 0 )
                {
                    reti = 2; /// PNG.
                }
                else
                if( strncmp( &testbuff[0], "BM", 2 ) == 0 )
                {
                    reti = 3; /// BMP.
                }

                if ( reti > 0 )
                {
                    *buff = new char[ flen ];
                    if ( *buff != NULL )
                    {
                        fread( *buff, 1, flen, fp );

                        if( buffsz != NULL )
                        {
                            *buffsz = flen;
                        }
                    }
                }
            }

            fclose( fp );
        }
    }

    return reti;
}
#endif

void WMain::loadImage()
{
    Fl_Native_File_Chooser nFC;

    nFC.title( "Select image file to load." );
    nFC.type( Fl_Native_File_Chooser::BROWSE_FILE );
    nFC.options( Fl_Native_File_Chooser::PREVIEW );
#if defined(__APPLE__)||defined(__linux__)
    nFC.filter( "Supported Image\t*.{png,jpg,jpeg,bmp}\n"
                "PNG Image\t*.png\n"
                "JPEG Image\t*.{jpg,jpeg}\n"
                "Windows BMP\t*.bmp" );
#else
    nFC.filter( "Supported Image\t*.png;*.jpg;*.jpeg;*.bmp\n"
                "PNG Image\t*.png;*.PNG\n"
                "JPEG Image\t*.jpg;*.JPG;*.JPEG;*.jpeg\n"
                "Windows BMP\t*.bmp;*.BMP" );
#endif /// of __APPLE__
    nFC.preset_file( "" );

    char refconvpath[1024] = {0,};

#ifdef SUPPORT_WCHAR
    if ( pathImage.size() > 0 )
    {
        fl_utf8fromwc( refconvpath, 1024,
                       pathImage.c_str(), pathImage.size() );
    }
    else
    {
        fl_utf8fromwc( refconvpath, 1024,
                       pathHome.c_str(), pathHome.size() );
    }
#else
    strcpy( refconvpath, pathHome.c_str() );
#endif
    nFC.directory( refconvpath );

    int retVal = nFC.show();

    if ( retVal == 0 )
    {
        string loadfn = nFC.filename();

        OnDropFiles( boxImgPreView, loadfn.c_str() );
    }
}

int WMain::sensFitting( const Fl_RGB_Image* img )
{
    // Makes current image to where fit
    if ( img != NULL )
    {
        if ( img->w() > img->h() )
        {
            return 0;
        }
        else
        if ( img->w() < img->h() )
        {
            return 1;
        }
    }

    //boxImgPreView->fitwidth();
    return 0;
}

bool WMain::saveImageProc()
{
    Fl_Native_File_Chooser nFC;

    static string presetfn;

    presetfn = imgFNameUTF8;
    presetfn += "_hdr.png";

    nFC.title( "Select PNG file to save." );
    nFC.type( Fl_Native_File_Chooser::BROWSE_SAVE_FILE );
    nFC.options( Fl_Native_File_Chooser::USE_FILTER_EXT
                 | Fl_Native_File_Chooser::SAVEAS_CONFIRM );
    nFC.filter( "PNG Image\t*.png" );
    nFC.preset_file( presetfn.c_str() );

    char refconvpath[1024] = {0};

    busyflag = true;

#ifdef _WIN32
    if ( pathImage.size() > 0 )
    {
        fl_utf8fromwc( refconvpath, 1024,
                       pathImage.c_str(), pathImage.size() );
    }
    else
    {
        fl_utf8fromwc( refconvpath, 1024,
                       pathHome.c_str(), pathHome.size() );
    }
#else
    if ( imgFNameUTF8.size() > 0 )
    {
        snprintf( refconvpath, 1024, "%s", imgFNameUTF8.c_str() );
    }
#endif

    nFC.directory( refconvpath );

    int retVal = nFC.show();

    if ( retVal == 0 )
    {
        imgFNameUTF8 = nFC.directory();

        savefn = nFC.filename(0);
        string testfn = removeFilePath( savefn.c_str() );

        if ( testfn.find_last_of('.') == string::npos )
        {
            savefn += ".png";
        }

        createThread( THREAD_JOB_SAVE_FILE );

        busyflag = false;
        return true;
   }

    busyflag = false;

    return false;
}

bool WMain::copy2clipboard( int cflag )
{
    // Copy to clipboard.
    if ( imgHDRed != NULL )
    {
        fl_cursor( FL_CURSOR_WAIT );

        unsigned max_w = imgHDRed->w();
        unsigned max_h = imgHDRed->h();

        Fl_RGB_Image* cloneImg = NULL;

        unsigned rszcp = 0;
        cfgLdr->GetResizeCopyFlag( rszcp );

        if ( ( rszcp == 0 ) || ( cflag > 0 ) )
        {
            cloneImg = (Fl_RGB_Image*)imgHDRed->copy();
        }
        else
        if ( rszcp == 1 )
        {
            // Too large image may not contain to clipboard !
            if ( ( max_w > 2048 ) || ( max_h > 2048 ) )
            {
                if ( max_w > 2048 )
                {
                    double rsratio = 2048.0 / (double)max_w;

                    max_w *= rsratio;
                    max_h *= rsratio;
                }
                else
                {
                    double rsratio = 2048.0 / (double)max_h;

                    max_w *= rsratio;
                    max_h *= rsratio;
                }
            }

            cloneImg = fl_imgtk::rescale( imgHDRed, max_w, max_h, fl_imgtk::BILINEAR );
        }
        
        if ( cloneImg == NULL )
        {
            fl_cursor( FL_CURSOR_DEFAULT );
            return false;
        }

        if ( CopyImageToClipboard( cloneImg ) == true )
        {

        }

        fl_imgtk::discard_user_rgb_image( cloneImg );
        fl_cursor( FL_CURSOR_DEFAULT );
        
        return true;
    }
    
    return false;
}

void WMain::rotateRight()
{
    if ( imgSource != NULL )
    {
        Fl_RGB_Image* rotImg = fl_imgtk::rotate90( imgSource );
        if ( rotImg != NULL )
        {
            fl_imgtk::discard_user_rgb_image( imgSource );
            imgSource = rotImg;
        }
    }

    if ( imgHDRed != NULL )
    {
        boxImgPreView->image( NULL );
        Fl_RGB_Image* newHDRed = fl_imgtk::rotate90( imgHDRed );
        if ( newHDRed != NULL )
        {
            fl_imgtk::discard_user_rgb_image( imgHDRed );
            imgHDRed = newHDRed;

            boxImgPreView->image( imgHDRed,sensFitting( imgHDRed ) );
            boxImgPreView->redraw();
        }
    }
    else
    {
        boxImgPreView->image( imgSource, sensFitting( imgSource ) );
        boxImgPreView->redraw();
    }

    updateImageHistogram();
}

void WMain::rotateLeft()
{
    if ( imgSource != NULL )
    {
        Fl_RGB_Image* rotImg = fl_imgtk::rotate270( imgSource );
        if ( rotImg != NULL )
        {
            fl_imgtk::discard_user_rgb_image( imgSource );
            imgSource = rotImg;
        }
    }

    if ( imgHDRed != NULL )
    {
        boxImgPreView->image( NULL );
        Fl_RGB_Image* newHDRed = fl_imgtk::rotate270( imgHDRed );
        if ( newHDRed != NULL )
        {
            fl_imgtk::discard_user_rgb_image( imgHDRed );
            imgHDRed = newHDRed;

            boxImgPreView->image( imgHDRed, sensFitting( imgHDRed ) );
            boxImgPreView->redraw();
        }
    }
    else
    {
        boxImgPreView->image( imgSource, sensFitting( imgSource ) );
        boxImgPreView->redraw();
    }

    updateImageHistogram();
}

bool WMain::createThread( unsigned idx )
{
    if ( idx < MAX_THREADS_CNT )
    {
        killThread( idx );

        threads[idx] = new ThreadParam;
        if ( threads[idx] != NULL )
        {
            threads[idx]->paramL = idx;
            threads[idx]->paramData = this;

            pthread_create( &threads[idx]->ptt,
                            NULL, WMain::PThreadCB,
                            threads[idx] );

            return true;
        }
    }
    return false;
}

void WMain::killThread( unsigned idx )
{
    if ( idx == THREAD_KILL_ALL )
    {
        for( unsigned cnt=0; cnt<MAX_THREADS_CNT; cnt++ )
        {
            killThread( cnt );
        }

        return;
    }
    else
    if ( idx < MAX_THREADS_CNT )
    {
        if ( threads[idx] != NULL )
        {
            pthread_t ptt = threads[idx]->ptt;

#ifdef __APPLE__
            if ( ptt != NULL )
#else
            if ( ptt > 0 )
#endif
            {
                pthread_cancel( ptt );
                pthread_join( ptt, NULL );
            }

            delete threads[idx];
            threads[idx] = NULL;
        }
    }
}

void WMain::TimerCall()
{
    if ( tmutex == true )
        return;

    tmutex = true;
        
    switch( timerEventID )
    {
        default:
            break;

        case 109: /// just updating boxProgress.
            Fl::lock();
            if ( boxProgress != NULL )
            {
                if ( boxProgress->visible_r() > 0 )
                    boxProgress->redraw();
            }
            window->redraw();
            Fl::unlock();
            Fl::awake();
            break;
            
        case 110: /// Notify ClipBoard copied.
            Fl::lock();
            if ( boxProgress != NULL )
            {
                boxProgress->label( strProgress.c_str() );
                if ( boxProgress->visible_r() == 0 )
                    boxProgress->show();
                else
                    boxProgress->redraw();
            }
            window->redraw();
            Fl::unlock();
            Fl::awake();
            // let lose overlay ...
            timerEventID = 120;
            Fl::repeat_timeout( 1.5f, WMain::TimerCB, this );
            break;

        case 120: /// Close clipboard notify.
            Fl::lock();
            if ( boxProgress != NULL )
                boxProgress->hide();
            window->redraw();
            Fl::unlock();
            Fl::awake();
            timerEventID = 130;
            Fl::repeat_timeout( 0.05f, WMain::TimerCB, this );
            break;

        case 130: /// Finalizing ...
            tstate = -1;
            break;
        
    }

    tmutex = false;
}

void WMain::PNGWriteCall(unsigned current, unsigned maxsz)
{
    if ( boxProgress != NULL )
    {
        if ( boxProgress->visible_r() > 0 )
        {
            static unsigned prev_progress = 0;
            bool redrawchance = false;
            float curperc = ( (float)current / (float) maxsz );
            char tmpstr[64] = {0};
            
            snprintf( tmpstr, 64,
                      "Writing in progress ... ( %.2f %% )",
                      curperc * 100.f );
                            
            unsigned curpercui = (unsigned)(curperc * 100.f);
            if ( curpercui != prev_progress )
            {
                prev_progress = curpercui;
                redrawchance = true;
            }

            strProgress = tmpstr;

            if ( redrawchance == true )
            {
                Fl::lock();
                boxProgress->label( strProgress.c_str() );
                boxProgress->redraw();
                window->redraw();
                Fl::unlock();
                Fl::awake();
            }
        }
    }
}
