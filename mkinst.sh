#!/bin/bash

replaceinfile()
{
    local fnsrc=$1
    local fndst=$2
    local dstr=$3
    local rstr=$4
    
    if [ -f ${fnsrc} ]; then
        sed "s|${dstr}|${rstr}|g" ${fnsrc} > ${fndst}
        return 1
    else
        return 0
    fi
}


VER_GREP=`grep APP_VERSION_STR res/resource.h`
VER_ARR=($VER_GREP)
VER_STR=`echo ${VER_ARR[2]} | sed "s/\"//g"`

INSTPATH=$1
DESKPATH=$2
APPNAME=$3

if [ "$INSTPATH" == "" ];then
    echo "Requires installation path."
    exit 0
fi

if [ "$DESKPATH" == "" ]; then
    DESKPATH=${HOME}/.local/share/applications
fi

if [ "$APPNAME" == "" ]; then
    echo "Error: requires parameter 3 for APPNAME."
    exit -1
fi

echo "Installing to ${INSTPATH} ... "

SRCRES=res/${APPNAME}.desktop
DSTRES=${DESKPATH}/${APPNAME}.desktop
TMPRES=_tmpres_

if [ ! -f ${SRCRES} ];then
    echo "Requires ${SRCRES} file."
    exit 0
fi

replaceinfile ${SRCRES} ${TMPRES} "##INSTALLEDPATH##" ${INSTPATH}
replaceinfile ${TMPRES} ${DSTRES} "##PROGRAMVERSION##" ${VER_STR}
rm -rf ${TMPRES}

echo "done."
