# Make Image O' HDR

## Require to build source.
1. Latest version of MinGW-W64 or Mac OS with llvm-gcc
1. Lastest version of fl_imgtk (debug & sse3(openmp option) )
1. Latest version of fltk-1.4.0.9 ( https://github.com/rageworx/fltk-custom )
1. Compuphase minIni ( https://github.com/compuphase/minIni )

## Updates in latest version 

* Version 0.2.27.98
    1. Windows HiDPI processing updated to native method instead of GDI+ processing.
    1. Icon image updated to SVG vector image.    
    1. FLTK-custom updated by latest changes.
	1. Enhanced displaying PNG write progress visual.

## Previous updates

* Version 0.2.25.90
    1. FLTK-custom updated to 1.4.0.9.
    1. Copying clipboard method for macOS updated to stanard of FLTK.
    1. Displaying clipboard actions are enhanced for all platforms.

* Version 0.2.24.86
    1. fixed high DPI makes strange blurred back of control pannel for Windows 10.

* Version 0.2.24.85
    1. new fl_imgtk version 0.3.39.25
	1. Reinhard HDRi performance issue fixed.

* Version 0.2.24.83
    1. FLTK upgraded to 1.4.0.6
    1. MacOS 11 support for universal binary
    1. Installer ( pkg ) for MacOS 11 ( 10.x is preparing ... )
* Version 0.2.24.81
    1. Supporting Windows high-DPI
* Version 0.2.23.80
    1. fl_imgtk library updated to 0.3.38.21.
	1. Fixed logmapping ( drago ) strange color correction.
	1. Fixed some codes for MacOS.
* Version 0.2.23.78
    1. Supporting wide charactors for asian charactors on Windows.
	1. Some UI improvements to displaying progress.
* Version 0.2.22.76
    1. Update fl_imgtk version 0.3.35.8
	2. Fixed drago HDR color space error.
* Version 0.2.22.75
    1. Fixing an issue for tool area not appear
* Version 0.2.22.74
    1. New button for reset parameters.
    1. Updated UI drawing time for small image now appears after processed HDR.
    1. hidden feature for controlling resized copying for over 2048px.
* Version 0.2.21.72
    1. Now supporting store current settings.
    1. external source code requires : minIni of compuphase.
* Version 0.2.16.66
    1. Blurback control panel came back for MacOSX.
	1. Built in library of FLTK upgraded for 1.3.5-2.
* Version 0.2.15.65
    1. Improved stability for MacOSX.
	1. Blurback control panel now just switched to dark transparency panel.
* Version 0.2.14.64
    1. Changed graphics update rule for each difference OS.
	1. Stability updated for Embedded Linux includes RaspberryPi3.
* Version 0.2.12.62
    1. Enhanced clipboard copying for MacOSX.
	1. Now it supports experimental static image clipboard copying.
* Version 0.2.12.60
    1. Enhanced clipboard copying for Windows.
	1. Mac OSX and Linux may next step for enhanced native clipboard copying.
* Version 0.2.11.58
    1. Fixed file opening dialog endfixes for Linux.
	1. Supporting latest FLTK-1.3.4.2-ts-ext3 flat scheme changes.
* Version 0.2.11.57
    1. Changed PNG compressing level 8 to 7 for speed.
* Version 0.2.11.56
    1. Changed to image now auto fitting
* Version 0.2.10.54
    1. Fixed a bug in export to PNG progress going to be crashed in Mac OSX
    1. Exporting progress now displays on window title on Mac OSX
* Version 0.2.10.52
    1. Fixed a bug clipboard image size not limited to 2048.
	1. Fixed some bugs that related in clipboard issues.
* Version 0.2.10.50-51
    1. Fixing library reference for Mac OS X
* Version 0.2.10.49
    1. Now supporting Mac OS X
	1. Fixing open dialog file extensions ( some bugs still left )
* Version 0.2.10.48
    1. Fixed an UI bug can not control adjusted parameter 3 and 4.
	1. Enhanced UI update sequence for Windows and Mac OS X
	1. Now enhancing for resource work for Mac OS X and multi platform.
* Version 0.2.9.46
    1. Fixed a bug that not activates button of copy to clipboard.
* Version 0.2.9.45
    1. Included some more buttons to easy control : Load image, rotatings.
	1. Updating UI sequence changed.
	1. Newly programmed of all graphical buttons.
	1. New PNG using deflate compression (zlib)
* Version 0.2.8.42
    1. Changed almost texts to images
	1. Images created from https://www.webpagefx.com/blog/web-design/free-icon-fonts-2/
	1. Changed some UI scales.
	1. Build target changed to SSE3 (arch=Core2Duo)
* Version 0.2.5.38
    1. Fixed some bugs from fl_imgtk.
	1. Added Copy to clipboard ( limit width or height is 2048px, automatically resized )
	1. Copying to clipboard works with ctrl + c keys.
	1. Rotate left and right with popup or ctrl + left or right keys.
* Version 0.2.0.32
    1. All internal processing moved to async pthread.
	1. Included eCLAHE.
* Version 0.1.6.20
    1. First commit for bitbucket repository.
	1. Prototypes in all fucntions.

## Supported OS
1. Windows NT 6.1 or above
1. Mac OS X El Capitan or above up to Majave.
1. Linux (May included Makefile.linux)
1. Rasberry Pi3 on board G++ compilation (use Makefile.rpi3)

## WIKI
* https://bitbucket.org/rageworx/make_img_o_hdr/wiki/
